<?php

use Phinx\Migration\AbstractMigration;
use Phinx\Util\Util;


class MigrationModel
{
    function __construct($options, $fields)
    {
        $this->options = $options;
        $this->fields = $fields;
    }
}


class MigrationStructureDelta
{
    const SAME      = 0x00;
    const CREATED   = 0x01;
    const MODIFIED  = 0x02;
    const CHILDREN  = 0x10;
    const DELETED   = 0x04;

    function __construct($target=null, $status=0)
    {
        $this->target = $target;
        $this->status = $status;
        $this->children = [];
    }

    function has_changes()
    {
        return $this->status != self::SAME;
    }

    function deleted_child($name, $target = null)
    {
        $this->status |= self::CHILDREN;
        if ( $target === null )
            $target = $name;
        $this->children[$name] = new MigrationStructureDelta($target, self::DELETED);
    }

    function child_delta($name, $delta)
    {
        if ( $delta->has_changes() )
            $this->status |= self::CHILDREN;
        $this->children[$name] = $delta;
    }

    function to_migration($indent="        ")
    {
        $migration = "";
        foreach ( $this->children as $delta )
        {
            $migration .= $delta->to_migration_model($indent);
        }
        return $migration;
    }

    function to_migration_model($indent = "        ", $istep = "    ")
    {
        if ( $this->status == self::SAME )
            return "";

        $model_name = $this->target;

        if ( $this->status == self::DELETED )
            return "\n$indent\$this->remove_model('$model_name');";

        $options = $model_name::to_migration_options();
        $migration = "";
        if ( $this->status == self::CREATED )
        {
            $migration = "\n$indent\$this->create_model('$model_name',\n";
            $migration .= string_array_to_string($options, true, $indent.$istep);
            $migration .= ",\n";
            $fields = [];
            foreach ( $model_name::fields() as $name => $field )
            {
                $options = $field->to_migration_options();
                if ( $options !== null )
                    $fields[$name] = $options;
            }
            $migration .= string_array_to_string($fields, true, $indent.$istep);
            $migration .= "\n$indent);";
            return $migration;
        }

        if ( $this->status & self::MODIFIED )
        {
            $migration = "\n$indent\$this->alter_model('$model_name',\n";
            $migration .= string_array_to_string($options, true, $indent.$istep);
            $migration .= "\n$indent);";
        }

        if ( $this->status & self::CHILDREN )
        {
            foreach ( $this->children as $delta )
            {
                $migration .= $delta->to_migration_field($indent);
            }
        }
        return $migration;
    }

    function to_migration_field($indent="        ")
    {
        if ( $this->status == self::SAME )
            return "";

        if ( $this->status == self::DELETED )
            return "\n$indent\$this->remove_field('{$this->target[0]}', '{$this->target[1]}');";

        $field_name = $this->target->field_name;
        $model_name = $this->target->model_name;
        $config = $this->target->to_migration_options();
        unset($config["column_type"]);
        $expected_str = string_array_to_string($config);
        if ( $this->status == self::CREATED )
            $method = "add_field";
        else
            $method = "alter_field";
        return "\n$indent\$this->$method('$model_name', '$field_name', $expected_str);";
    }
}

class MigrationStructure
{

    public $models = [];

    function add_model($name, $options, $fields)
    {
        $this->models[$name] = new MigrationModel($options, $fields);
    }

    function set_field($model, $field, $options)
    {
        $this->models[$model]->fields[$field] = $options;
    }

    function table_name($model)
    {
        return $this->models[$model]->options["table_name"];
    }

    function reset_field($model, $field, $options)
    {
        $old_options = $this->models[$model]->fields[$field];
        if ( $options !== null )
            $this->models[$model]->fields[$field] = $options;
        else
            unset($this->models[$model]->fields[$field]);
        return $old_options;
    }

    function get_field($model, $field)
    {
        return $this->models[$model]->fields[$field] ?? null;
    }

    function delta()
    {
        Model::load_models();
        $delta = new MigrationStructureDelta();
        $known_models = Model::known_models();
        foreach ( $known_models as $model )
        {
            $delta->child_delta($model, $this->delta_model($model));
        }

        foreach ( array_keys($this->models) as $model )
        {
            if ( array_search($model, $known_models) === false )
                $delta->deleted_child($model);
        }

        return $delta;
    }

    private function delta_model($model)
    {
        if ( !isset($this->models[$model]) )
            return new MigrationStructureDelta(
                $model,
                MigrationStructureDelta::CREATED
            );

        $delta = new MigrationStructureDelta($model);
        $model_data = &$this->models[$model];
        foreach ( $model::fields() as $field )
        {
            $delta_field = $this->delta_field($model, $field, $model_data);
            if ( $delta_field !== null )
                $delta->child_delta($field->field_name, $delta_field);
        }

        foreach ( array_keys($model_data->fields) as $field )
        {
            if ( !$model::has_field($field) )
                $delta->deleted_child($field, [$model, $field]);
        }

        if ( $model_data->options != $model::to_migration_options() )
            $delta->status |= MigrationStructureDelta::MODIFIED;

        return $delta;
    }

    private function delta_field($model, Field $field, MigrationModel $model_data)
    {
        $delta = new MigrationStructureDelta($field);

        $options = $field->to_migration_options();
        if ( $options === null )
            return null;

        if ( !isset($model_data->fields[$field->field_name]) )
            $delta->status = MigrationStructureDelta::CREATED;
        else if ( $options != $model_data->fields[$field->field_name] )
            $delta->status = MigrationStructureDelta::MODIFIED;

        return $delta;
    }
}

function popkey(&$array, $key)
{
    $val = $array[$key];
    unset($array[$key]);
    return $val;
}

abstract class BaseMigration extends AbstractMigration
{
    private $structure;

    function init()
    {
        $this->structure = new MigrationStructure();
        $this->structure_only = false;
    }

    function set_structure_only(MigrationStructure $structure)
    {
        $this->structure = $structure;
        $this->structure_only = true;
    }

    function create_model($name, $options, $fields)
    {
        $this->structure->add_model($name, $options, $fields);
        if ( $this->structure_only )
            return;

        $table_name = popkey($options, "table_name");
        $options["id"] = false;
        $table = $this->table($table_name, $options);
        foreach ( $fields as $name => $field_options )
        {
            $this->add_column($table, $field_options);
        }

        $table->create();
    }

    private function add_column($table, $field_options)
    {
        $column_name = popkey($field_options, "column_name");
        $has_column = popkey($field_options, "has_column");
        $unique = isset($field_options["unique"]) ? popkey($field_options, "unique") : false;

        if ( $has_column )
        {
            $type = popkey($field_options, 0);
            if ( $type == "foreign_key" )
            {
                $dest_model = $field_options[1];
                $dest_field = $field_options["to_field"];
                $related_options = $this->structure->get_field($dest_model, $dest_field);
                $dest_column = $related_options["column_name"];
                $related_options["column_name"] = $column_name;
                if ( isset($related_options["identity"]) )
                    unset($related_options["identity"]);

                $this->add_column($table, $related_options);
                $table->addForeignKey(
                    $column_name,
                    $this->structure->table_name($dest_model),
                    $dest_column,
                    # TODO Add options for these in the model field
                    ["delete"=>"CASCADE", "update"=>"CASCADE"]
                );
            }
            else
            {
//                 $type = popkey($field_options, "column_type");
                $table->addColumn($column_name, $type, $field_options);
            }


            if ( $unique )
            {
                $table->addIndex(
                    [$column_name],
                    ["unique" => true]
                );
            }
        }
    }

    function add_field($model, $field, $field_options)
    {
        $this->structure->set_field($model, $field, $field_options);
        if ( $this->structure_only )
            return;

        $table = $this->table_for_model($model);
        $this->add_column($table, $field_options);
        $table->update();
    }

    function rename_field($model, $old_name, $new_name)
    {
        $defined = $this->structure[$model];
        $defined->field[$new_name] = $defined->field[$old_name];
        unset($defined->field[$old_name]);
    }

    function alter_model($model, $options)
    {
        /// TODO
    }

    function alter_field($model, $field, $field_options)
    {
        $old_options = $this->structure->reset_field($model, $field, $field_options);
        if ( $this->structure_only )
            return;

        $had_column = popkey($old_options, "has_column");
        $has_column = popkey($field_options, "has_column");

        if ( !$has_column && !$had_column )
            return;

        $old_name = popkey($old_options, "column_name");
        $new_name = popkey($field_options, "column_name");

        $table = $this->table_for_model($model);

        if ( !$had_column )
        {
            $type = popkey($field_options, 0);
            $table->addColumn($new_name, $type, $field_options);
        }
        else if ( !$has_column )
        {
            $table->removeColumn($old_name);
        }
        else
        {
            if ( $old_name != $new_name )
                $table->renameColumn($old_name, $new_name);

            if ( $field_options != $old_options )
            {
                $type = popkey($field_options, 0);
                $table->changeColumn($old_name, $type, $field_options);
            }
        }

        $table->update();
    }

    function remove_field($model, $field)
    {
        $old_options = $this->structure->reset_field($model, $field, null);
        if ( $this->structure_only )
            return;

        $old_name = popkey($old_options, "column_name");
        $had_column = popkey($old_options, "has_column");
        if ( !$had_column )
            return;

        $table = $this->table_for_model($model);
        $table->removeColumn($old_name);
        $table->update();
    }

    function remove_model($model)
    {
        $table = $this->table_for_model($model);
        unset($this->structure->models[$model]);
        if ( $this->structure_only )
            return;
        $table->drop();
    }

    private function table_for_model($model)
    {
        return $this->table($this->structure->table_name($model));
    }

    abstract function change_models();

    function build_structure()
    {
        $rc = new ReflectionClass(static::class);
        $filename = $rc->getFileName();
        $version = Util::getVersionFromFileName($filename);
        $path = dirname($filename);
        $dependencies = [];
        foreach ( scandir($path) as $basename )
        {
            if ( Util::isValidMigrationFileName($basename) )
            {
                $dep_version = Util::getVersionFromFileName($basename);
                if ( $dep_version < $version )
                {
                    require_once("$path/$basename");
                    $classname = Util::mapFileNameToClassName($basename);
                    if ( class_exists($classname, False) && is_subclass_of($classname, self::class) )
                        $dependencies[$dep_version] = $classname;
                }
            }
        }
        ksort($dependencies);

        foreach ( $dependencies as $migration_class )
        {
            $migration = new $migration_class(
                $this->environment, $this->version, $this->input, $this->output
            );
            $migration->set_structure_only($this->structure);
            $migration->change_models();
        }
    }

    function change()
    {
        $this->build_structure();
        $this->change_models();
    }
}
