<?php

require_once("fields.php");
require_once("query.php");


abstract class Model
{
    static private $fields = [];
    protected $data = [];
    static $primary_key = "id";
    protected $id = ["integer", "identity"=>true, "default"=>0];

    static function to_migration_options()
    {
        return [
            "table_name" => static::table_name(),
        ];
    }

    function __construct($data=[], $from_db=false)
    {
        if ( $from_db )
        {
            $this->data = $data;
        }
        else
        {
            foreach ( $data as $name => $value )
            {
                $field = $this->get_unbound_field($name);
                $this->data[$field->column_name] = $field->php_to_db($value);
            }
        }
    }

    static function to_table_name($string)
    {
        return strtolower(
            preg_replace(
                "(([a-z]+[0-9]*)([A-Z]+))",
                "$1_$2",
                preg_replace(
                    "(([^a-zA-Z0-9_]))",
                    "",
                    str_replace(" ", "_", $string)
                )
            )
        );
    }

    static function table_name()
    {
        if ( isset(static::$table_name) )
            return static::$table_name;
        return self::to_table_name(static::class);
    }

    static function query()
    {
        return new Query(static::class);
    }

    function __get($name)
    {
        return $this->get_field($name)->get_value();
    }

    function __set($name, $value)
    {
        return $this->get_field($name)->set_value($value);
    }

    static function init_fields()
    {
        if ( !isset(self::$fields[static::class]) )
        {
            self::$fields[static::class] = [];
            $refl = new ReflectionClass(static::class);
            $default = $refl->getDefaultProperties();
            foreach ( $refl->getProperties(ReflectionProperty::IS_PROTECTED) as $prop )
            {
                if ( !isset($default[$prop->name]) )
                    continue;
                $value = $default[$prop->name];
                if ( is_array($value) && sizeof($value) > 0 && isset(Field::$types[$value[0]]) )
                {
                    self::$fields[static::class][$prop->name] = Field::get_field(static::class, $prop->name, $value);
                }

            }
        }
    }

    static function fields()
    {
        self::init_fields();
        return self::$fields[static::class];
    }

    static function add_dynamic_field(Field $field)
    {
        self::init_fields();
        self::$fields[static::class][$field->field_name] = $field;
    }

    static function has_field($name)
    {
        self::init_fields();
        return isset(self::$fields[static::class][$name]);
    }

    static function get_unbound_field($name) : Field
    {
        if ( !self::has_field($name) )
            throw new DbRuntimeError("Field $name not found in ".static::class);
        return self::$fields[static::class][$name];
    }

    function get_field($name) : BoundField
    {
        return new BoundField($this->get_unbound_field($name), $this->data);
    }

    public function raw_data()
    {
        return $this->data;
    }

    /**
     * \brief Similar to __get() but for the primary key
     * \returns \b null instead of raising an exception if the column is not set
     */
    function pk()
    {
        return $this->__get(static::$primary_key);
    }

    function pk_field() : BoundField
    {
        return $this->get_field(static::$primary_key);
    }

    function save($throw_on_error=true)
    {
        $pk_field = $this->pk_field();
        if ( $pk_field->has_value() )
            return $this->update($throw_on_error);
        else
            return $this->insert($throw_on_error);
    }

    function insert($throw_on_error=true)
    {
        $variables = [];
        $table_name = $this->table_name();
        $columns = [];
        $values = [];
        foreach ( $this->data as $column => $value)
        {
            $variables[":$column"] = $value;
            $columns []= "`$column`";
            $values []= ":$column";
        }
        $columns = implode(", ", $columns);
        $values = implode(", ", $values);
        $query = "insert into $table_name($columns) values ($values)";
        $statement = Connection::instance()->execute($query, $variables, $throw_on_error);

        $pk_field = $this->pk_field();
        if ( !$pk_field->has_value() )
            $pk_field->set_value(Connection::instance()->pdo->lastInsertId());

        return $statement !== null;
    }

    function update($throw_on_error=true)
    {
        $pk_field = $this->pk_field();
        $pk_column = $pk_field->field->column_name;
        $assignments = [];
        $variables = [":_pk" => $pk_field->get_value()];
        foreach ( $this->data as $column => $value)
        {
            if ( $column != $pk_column )
            {
                $assignments []= "$column = :$column";
                $variables[":$column"] = $value;
            }
        }
        $assignments = implode(", ", $assignments);
        $table_name = $this->table_name();
        $query = "update $table_name set $assignments where $pk_column = :_pk";
        return Connection::instance()->execute($query, $variables, $throw_on_error) !== null;
    }

    function delete($throw_on_error=true)
    {
        $pk_field = $this->pk_field();
        $pk_column = $pk_field->field->column_name;
        $variables = [":_pk" => $pk_field->get_value()];
        $table_name = $this->table_name();
        $query = "delete from $table_name where $pk_column = :_pk";
        return Connection::instance()->execute($query, $variables, $throw_on_error) !== null;
    }

    static function fetch($pk, $field = null) : Model
    {
        if ( $field === null )
            $field = static::$primary_key;
        return static::query()->where($field, '=', $pk)->select()->only();
    }

    /**
     * \brief Ensures all declared model classes have been initialized.
     *
     * This is particularly useful for making sure reverse relations are applied
     */
    static function load_models()
    {
        foreach ( get_declared_classes() as $class )
        {
            if ( is_subclass_of($class, self::class) )
                $class::init_fields();
        }
    }

    static function known_models()
    {
        return array_keys(self::$fields);
    }
}
