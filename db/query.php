<?php


require_once("connection.php");


class QueryResult implements Iterator, ArrayAccess
{
    public $model;
    private $statement;
    private $results;
    private $index = 0;

    function __construct($model, PDOStatement $statement)
    {
        $this->model = $model;
        $this->statement = $statement;
        $this->results = null;
    }

    function as_assoc()
    {
        $this->results = $this->statement->fetchAll(PDO::FETCH_ASSOC);
        return $this;
    }

    function as_array()
    {
        $this->results = $this->statement->fetchAll(PDO::FETCH_NUM);
        return $this;
    }

    function as_models()
    {
        $this->results = [];
        foreach ( $this->statement->fetchAll(PDO::FETCH_ASSOC) as $row )
        {
            $this->results []= new $this->model($row, true);
        }
        return $this;
    }

    function success()
    {
        return $this->statement;
    }

    function count()
    {
        $this->ensure_results();
        return sizeof($this->results);
    }

    function first($required = false)
    {
        if ( $this->count() == 0 )
        {
            if ( $required )
                throw new DbRuntimeError("Object not found");
            return null;
        }
        return $this->results[0];
    }

    function only()
    {
        if ( $this->count() > 1 )
            throw new DbRuntimeError("Too many objects");
        return $this->first(true);

    }

    private function ensure_results()
    {
        if ( $this->results === null )
            $this->as_models();
    }

    function sql()
    {
        return $this->statement->queryString;
    }

    function evaluated()
    {
        return $this->results !== null;
    }

    /**
     * \brief Returns the results as an array (based on which as_* function was called)
     */
    function results()
    {
        $this->ensure_results();
        return $this->results;
    }

// Iterator implementation
    #[\ReturnTypeWillChange]
    function current()
    {
        return $this->results[$this->index];
    }

    #[\ReturnTypeWillChange]
    function key()
    {
        return null;
    }

    function next(): void
    {
        $this->index++;
    }

    function rewind(): void
    {
        $this->index = 0;
        $this->ensure_results();
    }

    function valid(): bool
    {
        return $this->index < sizeof($this->results);
    }

// ArrayAccess
    function offsetExists($offset): bool
    {
        $this->ensure_results();
        return isset($this->results[$offset]);
    }

    #[\ReturnTypeWillChange]
    function offsetGet($offset)
    {
        $this->ensure_results();
        return $this->results[$offset];
    }

    function offsetSet($offset, $value): void
    {
        throw new Exception("Query results are read-only");
    }

    function offsetUnset($offset): void
    {
        throw new Exception("Query results are read-only");
    }
}

class QueryPaginator implements Iterator
{
    private $limit;
    private $offset = 0;
    private $result_set = null;
    private $page = 0;

    function __construct(Query $query, $paginate_by)
    {
        $this->query = $query;
        $this->limit = $paginate_by;
    }

    function set_page($index)
    {
        if ( $index != $this->page )
        {
            $this->page = $index;
            $this->offset = $this->page_offset($index);
            $this->result_set = null;
        }
    }

    function page_count()
    {
        return ceil($this->query->count() / $this->limit);
    }

    /**
     * \brief Number of items before the given page
     */
    function page_offset($page)
    {
        return $this->limit * $page;
    }

// Iterator implementation
    function current(): mixed
    {
        if ( $this->result_set === null )
            $this->result_set = $this->query->select($this->limit, $this->offset);
        return $this->result_set;
    }

    function key(): mixed
    {
        return $this->page;
    }

    function next(): void
    {
        $this->set_page($this->page + 1);
    }

    function rewind(): void
    {
        $this->set_page(0);
    }

    function valid(): bool
    {
        return $this->current()->count() > 0;
    }
}

class Query implements IteratorAggregate
{
    protected $where = [];
    protected $variables = [];
    protected $order_by = [];
    protected $joins = [];
    public $throw_on_error = false;

    function __construct($model, $throw_on_error=false)
    {
        $this->model = $model;
        $this->throw_on_error = $throw_on_error;
    }

    function throw_on_error($throw=true)
    {
        $this->throw_on_error = $throw;
        return $this;
    }

    function join_table($table_name, $on_field, $on_expression, $type="JOIN")
    {
        $name = $table_name;
        $i = 0;
        while(isset($this->joins[$name]))
        {
            $i++;
            $name = "$table_name$i";
        }

        $varname = $this->query_value($on_expression);
        $this->joins[$name] = "$type $table_name as $name on $name.$on_field = $varname";
        return $this;
    }

    function where($field_name, $operator, $value)
    {
        if ( strpos(".", $field_name) === false )
        {
            $column_name = $field_name;
            $field = null;
        }
        else
        {
            $field = $this->model::get_unbound_field($field_name);
            $column_name = $field->column_name;
        }

        $varname = $this->query_value($value, $field);
        $this->where []= "(`{$column_name}` $operator {$varname})";
        return $this;
    }

    private function query_value($value, $field = null)
    {
        if ( !($value instanceof QueryValue) )
        {
            if ( $field )
                $value = $field->php_to_db($value);
            $value = new QueryVariable(":_var" . sizeof($this->where), $value);
        }

        $value->populate_variables($this->variables);
        return $value->to_sql();
    }

    function paginate($paginate_by)
    {
        return new QueryPaginator($this, $paginate_by);
    }

    function count($limit = null, $offset = null)
    {
        $statement = $this->query("select count(*) from", $limit, $offset);
        return $statement->fetchColumn(0);
    }

    function select($limit = null, $offset = null)
    {
        $table_name = $this->model::table_name();
        $statement = $this->query("select `$table_name`.* from", $limit, $offset);
        return new QueryResult($this->model, $statement);
    }

    function delete($limit = null, $offset = null)
    {
        $statement = $this->query("delete from", $limit, $offset);
        return new QueryResult($this->model, $statement);
    }

    function first()
    {
        return $this->select(1)->first();
    }

    protected function query($query_head, $limit, $offset)
    {
        $table_name = $this->model::table_name();
        $query = "$query_head $table_name";

        $variables = $this->variables;

        if ( sizeof($this->joins) )
            $query .= "\n" . implode("\n", $this->joins);

        if ( sizeof($this->where) )
            $query .= "\nwhere\n" . implode(" and ", $this->where);

        if ( sizeof($this->order_by) )
            $query .= "\norder by\n" . implode(", ", $this->order_by);

        if ( $limit !== null )
        {
            $variables[":_limit"] = $limit;
            $query .= "\nlimit :_limit";
        }

        if ( $offset !== null )
        {
            $variables[":_offset"] = $offset;
            $query .= "\noffset :_offset";
        }

        return Connection::instance()->execute($query, $variables, $this->throw_on_error);
    }

// IteratorAggregate
    function getIterator(): Traversable
    {
        return $this->select();
    }
}

interface QueryValue
{
    function to_sql();
    function populate_variables(&$variables);
}

class QueryVariable implements QueryValue
{
    function __construct($name, $value)
    {
        $this->value = $value;
        $this->variable_name = $name;
    }

    function to_sql()
    {
        return $this->variable_name;
    }

    function populate_variables(&$variables)
    {
        $variables[$this->variable_name] = $this->value;
    }
}

class QueryColumn implements QueryValue
{
    function __construct($name)
    {
        $this->name = $name;
    }

    function to_sql()
    {
        return $this->name;
    }

    function populate_variables(&$variables)
    {
    }
}
