<?php

require_once(__dir__ . "/http.php");


function ensure_slash($string)
{
    if ( $string && substr($string, -1) != "/" )
        return "$string/";
    return $string;
}


class Telegram
{
    static private $auto_instance = null;
    public $api_uri = "https://api.telegram.org/";
    private $groups = [];
    private $sticker_sets = [];
    private $me = null;

    function __construct($token, $file_path, $file_url)
    {
        $this->token = $token;
        $this->file_path = ensure_slash($file_path);
        $this->file_url = ensure_slash($file_url);
    }

    /**
     * \return Telegram the telegram instance based on the global settings
    */
    static function instance()
    {
        if ( self::$auto_instance === null )
        {
            global $site;
            self::$auto_instance = new Telegram(
                $site->settings->telegram_token,
                $site->settings->root_dir . $site->settings->telegram_file_path,
                $site->settings->telegram_file_path
            );
        }
        return self::$auto_instance;
    }

    function get($method, $args=[])
    {
        $url = "{$this->api_uri}bot{$this->token}/$method?" . http_build_query($args);

        $contents = curl_get($url)[0];
        return json_decode($contents);
    }

    function group($name)
    {
        if ( !isset($this->groups[$name]) )
            $this->groups[$name] = new TelegramGroup($this, $name);
        return $this->groups[$name];
    }

    function sticker_set($name)
    {
        if ( !isset($this->sticker_sets[$name]) )
            $this->sticker_sets[$name] = new TelegramStickerSet($this, $name);
        return $this->sticker_sets[$name];
    }

    function download($file_path, $to)
    {
        $parent = dirname($to);
        if ( !is_dir($parent) && !mkdir($parent, 0755, true) )
            return null;

        $fp = fopen("{$this->api_uri}/file/bot{$this->token}/$file_path", "rb");
        if ( $fp )
        {
            $result = file_put_contents($to, $fp);
            fclose($fp);
            if ( $result === false )
            {
                unlink($to);
                return null;
            }
            return $to;
        }
        return null;
    }

    function me()
    {
        if ( $this->me == null )
            $this->me = new TelegramBotMe($this);
        return $this->me;
    }

    function bot($token)
    {
        return (new Telegram($token, $this->file_path, $this->file_url))->me();
    }

    function digest($string)
    {
        $secret_key = hash('sha256', $this->token, true);
        return hash_hmac('sha256', $string, $secret_key);
    }
}

class TelegramObject
{
    function __construct($client)
    {
        $this->client = $client;
    }
}

interface TelegramFetchable
{
    function refresh();
}

class TelegramGroup extends TelegramObject implements TelegramFetchable
{
    private $photo = null;

    function __construct($client, $name)
    {
        parent::__construct($client);
        $this->name = $name;
        $this->chat_id = "@" . strtolower($name);
        $this->link = "https://t.me/$name";

        $this->refresh();
    }

    function refresh()
    {
        $mc = $this->client->get(
            "getChatMembersCount",
            ["chat_id" => $this->chat_id]
        );
        if ( $mc->ok )
            $this->member_count = $mc->result;
        else
            $this->member_count = 0;

        $info = $this->client->get("getChat", ["chat_id" => $this->chat_id]);
        if ( $info->ok )
        {
            $this->id = $info->result->id;
            $this->title = $info->result->title;
            $this->description = $info->result->description;
            $this->photo = $info->result->photo;
        }
        else
        {
            $this->id = 0;
            $this->title = ucfirst($this->name);
            $this->description = "";
            $this->photo = null;
        }
    }

    function photo($thumb=false)
    {
        if ( $this->photo )
        {
            $size = $thumb ? "small" : "big";
            $id = "{$size}_file_id";
            $cache = "{$size}_file";
            if ( !isset($this->photo->$cache) )
                $this->photo->$cache = new TelegramFile($this->client, $this->photo->$id);
            return $this->photo->$cache;
        }
        return null;
    }

}

class TelegramPhotoSize extends TelegramObject
{
    protected $file = null;

    function __construct($client, $description)
    {
        parent::__construct($client);
        $this->file_id = $description->file_id;
        $this->width = $description->width;
        $this->height = $description->height;
        $this->file_size = $description->file_size;

    }

    function file()
    {
        if ( $this->file === null )
            $this->file = new TelegramFile($this->client, $this->file_id);
        return $this->file;
    }
}

class TelegramSticker extends TelegramPhotoSize
{
    function __construct($client, $description)
    {
        parent::__construct($client, $description);
        $this->emoji = $description->emoji;
        $this->set_name = $description->set_name;
        if ( isset($description->thumb) )
            $this->thumb = new TelegramPhotoSize($client, $description->thumb);
        else
            $this->thumb = $this;
    }

    function set()
    {
        return $this->client->sticker_set($this->set_name);
    }

    function png()
    {
        return new TelegramWebpToPng($this->client, $this->file());
    }
}

class TelegramStickerSet extends TelegramObject implements TelegramFetchable
{
    function __construct($client, $name)
    {
        parent::__construct($client);
        $this->name = $name;
        $this->link = "https://t.me/addstickers/$name";
        $this->refresh();
    }

    function refresh()
    {
        $info = $this->client->get("getStickerSet", ["name" => $this->name]);

        if ( $info->ok )
        {
            $this->title = $info->result->title;
            $this->contains_masks = $info->result->contains_masks;
            $this->stickers = [];
            foreach( $info->result->stickers as $sticker )
            {
                $this->stickers[] = new TelegramSticker($this->client, $sticker);
            }
        }
        else
        {
            $this->title = $this->name;
            $this->contains_masks = false;
            $this->stickers = [];
        }
    }

    function full_link($attrs=[])
    {
        return mkelement(["span", [], [
            new Link($this->link, $this->title, $attrs),
            " ",
            ["span", ["class"=>"telegram-stickers-count"], [
                "(", sizeof($this->stickers), " stickers)",
            ]],
        ]]);
    }
}

abstract class TelegramFileBase extends TelegramObject
{

    abstract protected function create_to($dest);

    function ensure_exists()
    {
        $dest = $this->server_filename(false);
        if ( !is_file($dest) )
            return $this->create_to($dest);
        return $dest;
    }

    function server_filename($ensure_exists=true)
    {
        if ( $ensure_exists )
            return $this->ensure_exists();
        return "{$this->client->file_path}{$this->path}";
    }

    function get_url($ensure_exists=true)
    {
        if ( $ensure_exists && $this->ensure_exists() === null )
            return null;
        return "{$this->client->file_url}{$this->path}";
    }
}

class TelegramFile extends TelegramFileBase implements TelegramFetchable
{
    function __construct($client, $file_id, $file_path=null)
    {
        parent::__construct($client);
        $this->file_id = $file_id;
        $this->file_size = null;
        $this->file_path = $file_path;

        if ( $file_path === null )
            $this->file_path = $this->find_existing();

        if ( $this->file_path === null )
            $this->refresh();
        else
            $this->path = $this->cleaned_filename();
    }

    protected function find_existing($path="")
    {
        $length = strlen($this->file_id);
        $base = $this->client->file_path . $path;
        foreach ( scandir($base) as $basename )
        {
            if ( substr($basename, 0, 1) == '.' )
                continue;

            $full = "$base$basename";
            if ( substr($basename, 0, $length) === $this->file_id )
                return "$path$basename";

            if ( is_dir($full) )
            {
                $found = $this->find_existing("$path$basename/");
                if ( $found )
                    return $found;
            }
        }

        return null;
    }

    function cleaned_filename()
    {
        $info = pathinfo($this->file_path);
        return "{$info['dirname']}/{$this->file_id}.{$info['extension']}";
    }

    function refresh()
    {
        $info = $this->client->get("getFile", ["file_id" => $this->file_id]);

        if ( $info->ok )
        {
            $this->file_path = $info->result->file_path;
            $this->file_size = $info->result->file_size;
            $this->path = $this->cleaned_filename();
        }
        else
        {
            $this->file_path = null;
            $this->file_size = null;
            $this->path = null;
        }
    }

    protected function create_to($dest)
    {
        return $this->client->download($this->file_path, $dest);
    }
}

class TelegramWebpToPng extends TelegramFileBase
{
    function __construct($client, $file)
    {
        parent::__construct($client);
        $this->file = $file;
        $info = pathinfo($this->file->file_path);
        $this->path = "{$info['dirname']}/{$this->file->file_id}.png";
    }

    protected function create_to($dest)
    {
        try
        {
            $fp = null;
            $source = $this->file->server_filename();
            $img = new Imagick($source);

            if ( !$img->valid() )
            {
                trigger_error("$source is not a valid image", E_USER_WARNING);
                return null;
            }

            $img->setImageFormat("png");

            $fp = fopen($dest, "wb");
            if ( !$fp )
            {
                trigger_error("Could not open $dest", E_USER_WARNING);
                return null;
            }

            $img->writeImageFile($fp);
            fclose($fp);
            return $dest;
        }
        catch(ImagickException $e)
        {
            trigger_error("Failed converting $source", E_USER_WARNING);
            trigger_error("Imagick error: " . $e->getMessage(), E_USER_WARNING);
            if ( $fp !== null )
            {
                fclose($fp);
                unlink($dest);
                if ( strpos($source, "\"") === false && strpos($dest, "\"") === false )
                {
                    trigger_error("Falling back to command line", E_USER_WARNING);
                    // For some reason it works from the command line but not in PHP
                    system("convert \"$source\" \"$dest\"", $return_code);
                    if ( $return_code != 0 )
                        trigger_error("Command failed with $return_code", E_USER_WARNING);
                }
            }
            return null;
        }
    }
}

class TelegramUser extends TelegramObject implements TelegramFetchable
{
    public $photo = null;

    function __construct($client, $user_id)
    {
        parent::__construct($client);
        $this->user_id = $user_id;
        $this->photo = null;
        $this->refresh();
    }


    function from_description($description)
    {
        $this->user_id = $description->id;
        $this->is_bot = $description->is_bot ?? false;

        $this->first_name = $description->first_name ?? null;
        $this->last_name = $description->last_name ?? null;

        if ( isset($description->username) )
        {
            $this->username = $description->username;
            $this->link = "https://t.me/{$this->username}";
        }
        else
        {
            $this->username = null;
            $this->link = null;
        }
    }

    function refresh()
    {
        $photos = $this->get_photos(1);
        if ( sizeof($photos) >= 1 )
            $this->photo = $photos[0];
    }

    function get_photos($limit=100, $offset=0)
    {
        $result = $this->client->get("getUserProfilePhotos", [
            "user_id" => $this->user_id,
            "limit" => $limit,
            "offset" => $offset,
        ]);
        if ( !$result->ok )
            return [];

        $photos = [];
        if ( $result->result->photos )
            foreach ( $result->result->photos[0] as $photo )
                $photos []= new TelegramPhotoSize($this->client, $photo);
        return $photos;
    }
}

class TelegramBotMe extends TelegramUser
{
    function __construct($client)
    {
        $this->client = $client;
        $this->refresh();
    }

    function refresh()
    {
        $me = $this->client->get("getMe");
        if ( $me->ok )
        {
            $this->from_description($me->result);
            parent::refresh();
        }
    }

    function login_button($auth_url, $send_messages=true, $size="large", $radius=20)
    {
        $auth_url = href($auth_url);

        $attrs = [
            "async" => "async",
            "src" => "https://telegram.org/js/telegram-widget.js?4",
            "data-telegram-login" => $this->username,
            "data-size" => $size,
            "data-radius" => $radius,
            "data-auth-url" => $auth_url,
        ];
        if ( $send_messages )
            $attrs["data-request-access"] = "write";

        return mkelement(["script", $attrs, ""]);
    }
}

class TelegramAuthedUser extends TelegramUser
{
    function __construct($client, $user_id, $photo_url, $data)
    {
        parent::__construct($client, $user_id);
        $this->from_description((object)$data);

        $this->display_name = trim("{$this->first_name} {$this->last_name}");
        if ( !$this->display_name )
        {
            if ( $this->username )
                $this->display_name = "Anonymous User #{$this->id}";
            else
                $this->display_name = $this->username;
        }

        $this->photo_url = $photo_url;
        $this->data = $data;
    }

    function refresh()
    {
    }

    function is_valid($max_age_seconds=24*60*60)
    {
        return self::check_auth($this->client, $this->data, $max_age_seconds);
    }

    static function check_auth($client, $data, $max_age_seconds=24*60*60)
    {
        if ( !isset($data["id"]) || !isset($data["hash"]) || !isset($data["auth_date"]) )
            return false;

        $hash = $data['hash'];
        unset($data['hash']);

        $data_check = [];
        foreach ($data as $key => $value)
            $data_check[] = "$key=$value";

        sort($data_check);

        $data_check_string = implode("\n", $data_check);

        if ( $hash !== $client->digest($data_check_string) )
            return false;

        if ( time() - $data['auth_date']  > $max_age_seconds )
            return false;

        return true;
    }

    static function from_data($data, $client=null, $max_age_seconds=24*60*60)
    {
        if ( $client === null )
            $client = Telegram::instance();

        if ( !self::check_auth($client, $data, $max_age_seconds) )
            return null;

        $photo = $data["photo_url"] ?? "";
        return new TelegramAuthedUser($client, $data["id"], $photo, $data);
    }
}
