<?php

require_once(__dir__.'/html.php');

class License extends PageWidget
{
    static public $licenses = array();
    static public $use_buttons = false;

    function __construct($name, $url, $button)
    {
        $this->name = $name;
        $this->url = $url;
        $this->button = $button;
    }

    function as_link($content=null)
    {
        if ( $content == null )
            $content = $this->name;
        return new Link($this->url, $content, ["rel"=>"license"]);
    }

    function as_image()
    {
        return new SimpleElement(
            "img",
            array(
                "src" => href($this->button),
                "title" => $this->name,
                "alt" => $this->name,
            )
        );
    }

    function element()
    {
        return $this->as_link(self::$use_buttons && $this->button ? $this->as_image() : $this->name);
    }

    function meta_element()
    {
        return mkelement(["link", ["rel" => "license", "content" => $this->url]]);
    }

    static function find_license($name)
    {
        foreach(License::$licenses as $license)
        {
            if ( $license->name == $name )
                return $license;
        }

        return new License($name, "", null);
    }
}

License::$licenses = array(
    new License(
        "AGPLv3+",
        "https://www.gnu.org/licenses/agpl-3.0.en.html",
        "https://www.gnu.org/graphics/agplv3-88x31.png"
    ),
    new License(
        "GPLv3+",
        "https://www.gnu.org/licenses/gpl-3.0.en.html",
        "https://www.gnu.org/graphics/gplv3-88x31.png"
    ),
    new License(
        'CC BY-SA',
        'https://creativecommons.org/licenses/by-sa/4.0/',
        'https://licensebuttons.net/l/by-sa/4.0/88x31.png'
    ),
    new License(
        'CC BY',
        'https://creativecommons.org/licenses/by/4.0/',
        'https://licensebuttons.net/l/by/4.0/88x31.png'
    ),
    new License(
        'CC0',
        'https://creativecommons.org/publicdomain/zero/1.0/',
        null
    ),
    new License(
        'CC BY-NC-SA',
        'http://creativecommons.org/licenses/by-nc-sa/4.0/',
        'https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png'
    ),
    new License(
        'CC BY-NC-ND',
        'http://creativecommons.org/licenses/by-nc-nd/4.0/',
        'https://i.creativecommons.org/l/by-nc-nd/4.0/88x31.png'
    ),
);

