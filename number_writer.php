<?php

/**
 * \file
 * Number to english translation
 * \see rawr.js
 */
class NumberWriterComponent
{
    function __construct($factor, $values)
    {
        $this->factor = $factor;
        $this->values = $values;
    }

    function get_for($number)
    {
        if ( $number < $this->factor )
            return null;
        $remainder = $number % $this->factor;
        $multiplier = floor($number / $this->factor);
        $text = $this->values[$multiplier-1] ?? null;

        if ( $text === null )
        {
            $text = $this->values[0];
            if ( $text === null )
                return null;
        }
        else if ( $multiplier != 1 )
        {
            $multiplier = null;
        }

        if ( $this->factor == 1 )
        {
            $remainder = 0;
            $multiplier = 0;
        }

        return (object) [
            "remainder" => $remainder,
            "multiplier" => $multiplier,
            "text" => $text,
        ];
    }
}

class NumberWriter
{
    function  __construct($zero, $components)
    {
        $this->zero = $zero;
        $this->components = $components;
    }

    function format_number($number)
    {
        if ( $number == 0 )
            return $this->zero;
        if ( $number < 0 )
            return $this->format_number(-$number);

        $result = "";

        foreach ( $this->components as $comp )
        {
            $partial = $comp->get_for($number);

            if ( $partial === null )
                continue;

            $number = $partial->remainder;
            if ( $partial->multiplier )
                $result = $this->join($result, $this->format_number($partial->multiplier));
            $result = $this->join($result, $partial->text);

            if ( !$number )
                break;
        }

        return $result;
    }

    function join($result, $str)
    {
        if ( $result )
            $result .= " ";
        return $result . $str;
    }
}

function rawr_number_writer()
{
    return new NumberWriter("zerdurg", [
        new NumberWriterComponent(1000000000, ["dergbillion"]),
        new NumberWriterComponent(1000000, ["durgillion"]),
        new NumberWriterComponent(1000, ["clawsand"]),
        new NumberWriterComponent(100, ["hunderg"]),
        new NumberWriterComponent(10,
        [null, "twenderg", "thirderg", "fourderg", "fiftderg", "sixderg",
        "sevenderg", "eightderg", "ninederg"]),
        new NumberWriterComponent(1,
            ["dragone", "twurg", "three", "roar", "five", "six", "severg", "eight", "nine", "derg",
            "dergeven", "dergwelve", "dergeen", "fourdergeen", "fifdergeen", "sixdergeen",
            "sevendergeen", "eightdergeen", "ninedergeen"]),
    ]);
}
