<?php

require_once(__dir__."/../page.php");
require_once(__dir__."/../http.php");


/**
 * \brief Custom redirection page
 */
class RedirectPage extends Page
{
    /**
     * \param $uri          Location to redirect to
     * \param $status       Response status
     * \param $content      Response body (to override redirect_message from settings)
     * \param $content_type Content-Type header
     */
    function __construct($uri, $status=302, $content = null, $content_type = "text/plain; charset=utf-8")
    {
        $this->location = $uri;
        $this->status = $status;
        $this->content = $content;
        $this->content_type = $content_type;
    }

    /**
     * \returns The URI to redirect the path to
     */
    function get_location($path)
    {
        return $this->location;
    }

    function dispatch($path)
    {
        $location = $this->get_location($path);
        redirect($location, $this->status);
        $this->render(array("from" => $path, "to" => $location));
        return true;
    }

    function render($render_args=array())
    {
        header("Content-Type: {$this->content_type}");
        global $site;
        echo $this->content ?? $site->settings->redirect_message;
    }
}

/**
 * \brief Redirect adding a trailing slash
 */
function redirecto_to_slash($status=301)
{
    $request_uri = explode("?", $_SERVER["REQUEST_URI"]);
    $url = $request_uri[0];
    $query = isset($request_uri[1]) ?  "?" . $request_uri[1] : "";
    if ( $url == "" || $url[strlen($url)-1] != "/" )
    {
        return new RedirectPage($url."/".$query, $status);
    }

    trigger_error("$url already ends with a slash", E_USER_WARNING);
}
