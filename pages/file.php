<?php

require_once(__dir__."/../util.php");

/**
 * \brief Page mixin that lists files under a certain path a path
 */
trait DirListTrait
{
    /**
     * \brief Prefix to prepend to all path
     * (eg: to make them look like they are from as specific location)
     */
    function path_prefix()
    {
        return "";
    }

    function dispatch($path)
    {
        if ( !$path || $path[strlen($path)-1] != "/" )
            return $this->dispatch_noslash($path);

        if ( strpos($path, "../") === false && is_dir($this->full_path($path)) )
        {
            $path = preg_replace("(//+)", "/", $path);
            $this->render(array("path"=>$path));
            return true;
        }

        return false;
    }

    /**
     * \brief Returns the full server-side path given a suffix relative to path_prefix()
     */
    function full_path($relative)
    {
        return path_join($this->base_path(), $relative);
    }

    /**
     * \brief Returns the full client-facing URL given a suffix relative to path_prefix()
     */
    function full_url($relative)
    {
        return path_join($this->base_url(), $relative);
    }

    function title($render_args=array())
    {
        return "{$this->title} " . path_join($this->path_prefix(), $render_args["path"]);
    }

    function skip_item($dirname, $filename)
    {
        return true;
    }

    function main($render_args)
    {
        $local_path = trim($render_args["path"], '/');
        $full_path = $this->full_path($local_path);
        $dirs = array();
        $files = array();
        foreach ( scandir($full_path) as $filename )
        {
            if ( $filename == "." || $filename == ".." || $this->skip_item($local_path, $filename) )
                continue;

            if ( is_dir("$full_path/$filename") )
                $dirs []= "$filename/";
            else
                $files []= $filename;
        }

        $this->render_file_list_title($local_path, $render_args);
        $this->render_file_list_begin($local_path, $render_args);

        if ( $local_path )
            $this->render_file_list_item("Parent", "parent", dirname($local_path));

        foreach ( $dirs as $item )
        {
            $this->render_file_list_item("$item", "directory", "$local_path/$item");
        }

        foreach ( $files as $item )
        {
            $file_path = "$local_path/$item";
            $file_type = $this->get_file_type($file_path);
            $this->render_file_list_item($item, $file_type, $file_path);
        }

        $this->render_file_list_end($local_path, $render_args);
    }

    /**
     * \brief Returns the type of file, for custom styling when displaying it
     */
    function get_file_type($path)
    {
        return "file";
    }

    /**
     * \brief Renders a single file of type \p $type
     */
    function render_file_list_item($name, $type, $path)
    {
        echo "<li class='file_$type'>".(new Link($this->full_url($path), $name))."</li>";
    }

    /**
     * \brief Renders the start of the list of files
     */
    function render_file_list_begin($path, $render_args)
    {
        echo "<ul class='file_list'>";
    }

    /**
     * \brief Renders the end of the list of files
     */
    function render_file_list_end($path, $render_args)
    {
        echo "</ul>";
    }

    /**
     * \brief Renders the title / breadcrumbs
     */
    function render_file_list_title($path, $render_args)
    {
        echo "<nav class='path'>";

        if ( $this->title )
        {
            echo mkelement(["span", array("class"=>"path_title"), $this->title]);
        }

        $separator = new SimpleElement("span", array("class"=>"path_sepatator"), "/");
        $link_attrs = array("class"=>"path_component");

        echo $separator;
        $global_prefix = $this->path_prefix();
        if ( $global_prefix )
        {
            echo new Link($global_prefix, trim($global_prefix, "/"), $link_attrs);
            echo $separator;
        }

        if ( $path )
        {
            $prefix = $this->base_url();
            $components = explode("/", $path);
            foreach ( $components as $component )
            {
                $prefix = path_join($prefix, "$component/");
                echo new Link($prefix, $component, $link_attrs);
                echo $separator;
            }
        }
        echo "</nav>";
    }

    /**
     * \returns The server-side directory path that is being scanned
     */
    function base_path()
    {
        return path_join($this->self_dirname(), $this->path_prefix());
    }

    /**
     * \brief Returns the base url for links
     */
    function base_url()
    {
        return $this->path_prefix();
    }
}
