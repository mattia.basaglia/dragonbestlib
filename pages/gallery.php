<?php


function path_encode($filename)
{
    return str_replace("%2F", "/", rawurlencode($filename));
}

abstract class MediaFileInfo
{
    public $page, $filename, $slug, $title, $extension, $meta;

    /**
     * \param $page      Page object owning the image
     * \param $filename  (relative) filename of the media
     * \param $slug      Short unique name, to link to the details page
     * \param $extension File extension
     */
    function __construct($page, $filename, $slug, $title, $extension=null, $meta=[])
    {
        $this->page = $page;
        $this->filename = $filename;
        $this->slug = $slug;
        $this->title = $title;
        $this->extension = $extension === null ? pathinfo($filename, PATHINFO_EXTENSION) : $extension;
        $this->meta = $meta;

        if ( !isset($this->meta["name"]) )
            $this->meta["name"] = $title;
    }

    abstract function is_visual();

    protected function hidden_meta()
    {
        return ["name", "license", "author", "copyrightYear"];
    }

    /**
     * \brief Renders the HTML for a thumbnail / preview of the item
     */
    function render_thumbnail(
        $css_class,
        $link,
        $outer_attrs=[],
        $image_attrs=["property" => "image"],
        $hidden_meta=null
    )
    {
        $this->render_image(
            $this->thumbnail_url(),
            $css_class,
            $link,
            $outer_attrs,
            $image_attrs,
            $hidden_meta
        );
    }

    /**
     * \brief Renders the HTML to display the item
     */
    abstract function render($css_class, $outer_attrs, $hidden_meta);

    protected function render_wrapper($element, $css_class, $link, $outer_attrs, $hidden_meta)
    {
        $outer_attrs["class"] = $css_class;
        $element_tag = "span";

        if ( $link )
        {
            $outer_attrs["href"] = href($link);
            $element_tag = "a";
        }

        $inner_elements = array_merge([$element], $this->render_hidden_meta($hidden_meta));

        echo mkelement([$element_tag, $outer_attrs, $inner_elements]);
    }

    /**
     * \brief Renders an HTML image
     * \param $image_url    Image to display
     * \param $css_class    CSS class name of the containing element
     * \param $link         (optional) link url, if present a <a> element will
     *                      wrap the image; if omitted, it will be a <span>
     * \param $outer_attrs  Attributes of the outer element
     * \param $image_attrs  Attributes of the <img> element
     * \param $hidden_meta  Which metadata to render for the image
     */
    protected function render_image(
        $image_url,
        $css_class,
        $link,
        $outer_attrs=[],
        $image_attrs=[],
        $hidden_meta=null
        )
    {
        $img = new SimpleElement("img",
            array_merge([
                "src" => href($image_url),
                "alt" => $this->title,
                "title" => $this->title,
            ], $image_attrs
            )
        );
        $this->render_wrapper($img, $css_class, $link, $outer_attrs, $hidden_meta);
    }

    protected function render_hidden_meta($hidden_meta)
    {
        if ( $hidden_meta === null )
            $hidden_meta = $this->hidden_meta();

        $inner_elements = [];
        foreach ( $hidden_meta as $prop )
        {
            if ( ! isset($this->meta[$prop]) )
                continue;
            $value = $this->meta[$prop];
            if ( $value instanceof License )
                $inner_elements[] = new SimpleElement(
                    "link",
                    ["rel" => $prop, "content" => $value->url]
                );
            else
                $inner_elements[] = new SimpleElement(
                    "meta",
                    ["property" => $prop, "content" => $value]
                );

        }
        return $inner_elements;
    }

    /**
     * \brief Returns the full URI of the media object
     */
    function full_url()
    {
        return href(path_join($this->page->media_path(), path_encode($this->filename)));
    }

    /**
     * \brief Returns the URI for an image preview of the media object
     */
    function thumbnail_url()
    {
        return $this->full_url();
    }

    function details_url()
    {
        return href(path_join($this->page->base_uri(), path_encode($this->slug)) . "/");
    }
}

class AudioInfo extends MediaFileInfo
{
    /**
     * \param $page      Page object owning the image
     * \param $filename  (relative) filename of the image
     * \param $slug      Short unique name, to link to the details page
     * \param $extension File extension
     */
    function __construct($page, $filename, $slug, $title, $extension=null, $meta=[])
    {
        parent::__construct($page, $filename, $slug, $title, $extension, $meta);

        if ( !isset($this->meta["itemtype"]) )
            $this->meta["itemtype"] = "AudioObject";
    }

    function thumbnail_url()
    {
        return href("/media/img/rasterized/icon/icon_audio.png");
    }

    function is_visual()
    {
        return false;
    }

    function mime()
    {
        $type = $this->extension;
        if ( $type == "mp3" )
            $type = "mpeg";
        return "audio/$type";
    }

    function render($css_class, $outer_attrs, $hidden_meta)
    {
        $this->render_image(
            $this->thumbnail_url(),
            $css_class,
            $this->full_url(),
            $outer_attrs,
            [],
            $hidden_meta
        );

        echo mkelement([
            "audio",
            ["controls" => "controls", "src" => $this->full_url()],
            []
        ]);
    }
}

class VideoInfo extends MediaFileInfo
{
    public $thumb;

    /**
     * \param $page      Page object owning the image
     * \param $filename  (relative) filename of the image
     * \param $slug      Short unique name, to link to the details page
     * \param $extension File extension
     */
    function __construct($page, $filename, $thumb, $slug, $title, $extension=null, $meta=[])
    {
        parent::__construct($page, $filename, $slug, $title, $extension, $meta);
        $this->thumb = $thumb;

        if ( !isset($this->meta["itemtype"]) )
            $this->meta["itemtype"] = "VideoObject";

        if ( !isset($this->meta["thumbnail"]) )
            $this->meta["thumbnail"] = $this->thumbnail_url();

        if ( !isset($this->meta["thumbnailUrl"]) )
             $this->meta["thumbnailUrl"] = $this->meta["thumbnail"];

        if ( !isset($this->meta["name"]) )
             $this->meta["name"] = $slug;

        if ( !isset($this->meta["description"]) )
             $this->meta["description"] = $title;

        if ( !isset($this->meta["uploadDate"]) )
        {
            $date = $this->meta["dateCreated"] ?? date("Y-m-d", filemtime(path_join($this->page->server_image_path(), $filename)));
            $this->meta["uploadDate"] = $date;
        }
    }

    function thumbnail_url()
    {
        return href(path_join($this->page->media_path(), path_encode($this->thumb)));
    }

    function is_visual()
    {
        return true;
    }

    function mime()
    {
        $type = $this->extension;
        if ( $type == "ogv" )
            $type = "ogg";
        else if ( $type == "mp4" )
            $type = "mpeg";
        return "video/$type";
    }

    function render($css_class, $outer_attrs, $hidden_meta)
    {
        $video = mkelement([
            "video",
            [
                "controls" => "controls",
                "src" => $this->full_url(),
                "loop" => "loop",
                "poster" => $this->thumbnail_url(),
            ],
            []
        ]);

        $outer_attrs["class"] = $css_class;
        $inner_elements = array_merge([$video], $this->render_hidden_meta($hidden_meta));
        unset($outer_attrs["property"]);
        echo mkelement(["span", $outer_attrs, $inner_elements]);
    }


    protected function hidden_meta()
    {
        return array_merge(parent::hidden_meta(), ["uploadDate", "description", "thumbnailUrl"]);
    }
}

/**
 * \brief Metadata for a single image
 */
class ExplicitImageInfo extends MediaFileInfo
{
    /**
     * \param $page      Page object owning the image
     * \param $filename  (relative) filename of the image
     * \param $slug      Short unique name, to link to the details page
     * \param $extension File extension
     */
    function __construct($page, $filename, $slug, $title, $extension=null, $meta=[])
    {
        parent::__construct($page, $filename, $slug, $title, $extension, $meta);

        if ( !isset($this->meta["itemtype"]) )
            $this->meta["itemtype"] = "ImageObject";
    }

    function render_custom_link($css_class, $outer_attrs, $hidden_meta, $link)
    {
        $this->render_image(
            $this->full_url(),
            $css_class,
            $link,
            $outer_attrs,
            [],
            $hidden_meta
        );
    }

    function render($css_class, $outer_attrs, $hidden_meta)
    {
        $this->render_image(
            $this->full_url(),
            $css_class,
            $this->full_url(),
            $outer_attrs,
            [],
            $hidden_meta
        );
    }

    /**
     * \brief Returns the full URI of the image;
     *      if it's a vector image, it will use the rasterized path to get a
     *      rasterized version of the image.
     */
    function thumbnail_url()
    {
        if ( $this->extension !== ".png" )
        {
            $raster_path = $this->page->raster_image_path();
            if ( $raster_path !== null )
            {
                return href(path_join($raster_path, str_replace($this->extension, ".png", path_encode($this->filename))));
            }
        }
        return $this->full_url();
    }

    function is_visual()
    {
        return true;
    }
}

class ImageInfo extends ExplicitImageInfo
{
    /**
     * \param $page      Page object owning the image
     * \param $filename  (relative) filename of the image
     * \param $slug      Short unique name, to link to the details page
     * \param $extension File extension
     */
    function __construct($page, $filename, $slug, $title, $extension)
    {
        $meta = [
            "author" => $page->copy_author,
            "copyrightYear" => $page->copy_range(),
            "license" => $page->license_object(),
        ];
        parent::__construct($page, $filename, $slug, $title, $extension, $meta);
    }
}

class LottieImageInfo extends ImageInfo
{

    function render($css_class, $outer_attrs, $hidden_meta)
    {
        $widget = $this->render_lottie_widget($this->full_url());
        $this->render_wrapper($widget, $css_class, null, $outer_attrs, $hidden_meta);
    }

    function render_lottie_widget($image_url)
    {
        $id = "lottie_" . spl_object_hash($this);
        return mkelement(["div", [], [
            ["div", ["class" => "lottie alpha-back", "id"=> $id, "onclick" => "toggle_$id();"], ""],
            ["script", [], [new HtmlString("
                var anim_$id = bodymovin.loadAnimation({
                    container: document.getElementById('$id'),
                    renderer: 'svg',
                    loop: true,
                    autoplay: true,
                    path: '$image_url',
                });
                function speed_$id()
                {
                    var anim = anim_$id;

                    if ( anim.playSpeed < 1 )
                        anim.setSpeed(1);
                    else if ( anim.playSpeed < 2 )
                        anim.setSpeed(2);
                    else
                        anim.setSpeed(0.5);

                    if ( anim.isPaused )
                        toggle_$id();
                }
                function toggle_$id()
                {
                    anim_$id.togglePause();
                    var togbutt = document.getElementById('pause_$id');
                    togbutt.children[0].classList.toggle('fa-play');
                    togbutt.children[0].classList.toggle('fa-pause');
                    togbutt.setAttribute('title', anim_$id.isPaused ? 'Play' : 'Pause');
                }
            ")]],
            ["ul", ["class" => "buttons"], [
                ["li", [], [["a", ["onclick" => "toggle_$id();", "title" => "Pause", "id"=>"pause_$id"], [["span", ["class"=>"fas fa-pause"], []]]]]],
                ["li", [], [["a", ["onclick" => "speed_$id();", "title" => "Speed"], [["span", ["class"=>"fas fa-forward"], []]]]]],
                ["li", [], [["a", ["href" => $this->full_url() . "?download", "title" => "Download"], [["span", ["class"=>"fas fa-download"], []]]]]],
            ]],
        ]]);
    }
}

/**
 * \brief A collection of ImageInfo with an (optional) title
 */
class ImageGroup
{
    public $name, $images;

    function __construct($name, $images = [])
    {
        $this->name = $name;
        $this->images = $images;
    }

    function add(MediaFileInfo $info)
    {
        $this->images []= $info;
    }
}

/**
 * \brief Mixin to render a gallery of images
 */
trait GalleryTrait
{
    public $image_groups;

    function __construct()
    {
        parent::__construct();
        $this->patterns["^/(?P<image>.*[^/])/?$"] = "details";
        $this->image_groups = $this->load_images();
    }

    /// Path to search images in (trailing part)
    abstract function media_path();

    /**
     * \brief Path containing rasterized SVG images
     * \note If \b null no rasterized urls will be used
     */
    function raster_image_path()
    {
        return null;
    }

    /**
     * \brief Beginning of the uri to the page to generate links
     */
    abstract function base_uri();

    /**
     * \param $selected 0 = normal, 1 = selected, 2 = main image
     * \returns The CSS class(es) for an image
     */
    protected function get_css_class($selected)
    {
        if ( $selected == 1 ) return "image selected";
        if ( $selected == 2 ) return "image big";
        return "image";
    }

    /**
     * \brief Loads images
     * \returns An array of image groups
     */
    abstract function load_images();

    /**
     * \brief Should populate \p $image->meta.
     */
    protected function load_image_metadata($image)
    {
    }

    /**
     * \returns The matching image or \b null
     */
    function find_image($slug)
    {
        foreach ( $this->image_groups as $group )
        {
            foreach ( $group->images as $image )
            {
                if ( $image->slug == $slug )
                    return $image;
            }
        }
        return null;
    }

    /**
     * \brief Invoked when displaying a specific image
     */
    function details($match)
    {
        $image = $this->find_image(urldecode($match["image"]));
        if ( !$image )
        {
            http_response_code(404);
        }
        $this->render(array("image" => $image));
    }

    function title($render_args=array())
    {
        return $render_args["image"]->meta["title"] ?? $render_args["image"]->title ?? $this->title;
    }

    function get_meta_description($render_args=array())
    {
        return $render_args["image"]->meta["description"] ?? $this->description;
    }

    function get_meta_type($render_args)
    {
        return isset($render_args["image"]) ? "photo" : null;
    }

    function main($render_args)
    {
        $focus = $render_args["image"] ?? null;
        $this->render_gallery($focus, $render_args);
    }

    /**
     * \brief Renders the whole gallery
     */
    function render_gallery($focus, $render_args)
    {
        if ( $focus )
            $this->render_gallery_focus($focus, $render_args);

        $this->render_gallery_thumbnails($focus, $render_args);
    }

    protected function render_gallery_focus($focus, $render_args)
    {
        echo "<div class='focus' typeof='{$focus->meta['itemtype']}'>";
        $this->render_focused_title($focus, $render_args);
        $this->render_focused($focus, $render_args);
        echo '</div>';
    }

    protected function render_gallery_thumbnails($focus, $render_args)
    {
        foreach ( $this->image_groups as $group )
        {
            echo '<div class="gallery-wrapper" typeof="ImageGallery">';
            $this->render_group_title($group, $render_args);

            echo '<div class="gallery">';
            foreach ( $group->images as $image )
            {
                if ( $focus && $image->filename == $focus->filename )
                    $this->render_thumbnail_focused($image, $render_args);
                else
                    $this->render_thumbnail($image, $render_args);
            }
            echo '</div>';
            echo '</div>';
        }
    }

    /**
     * \brief Renders title for the currently selected image
     */
    function render_focused_title($image, $render_args)
    {
        echo mkelement(['h1', ["property" => "name"], $image->title]);
    }

    /**
     * \brief Renders title an image group
     */
    function render_group_title($group, $render_args)
    {
        if ( $group->name )
            echo mkelement([
                "h2",
                ["class"=>"gallery-title", "property" => "name"],
                $group->name
            ]);
    }

    /**
     * \brief Returns the metadata to show for a given thumbnail
     */
    function thumbnail_meta(MediaFileInfo $image)
    {
        return [
            "property" => "associatedMedia",
            "typeof" => $image->meta["itemtype"],
        ];
    }

    /**
     * \brief Renders the thumbnail for the currently selected image
     */
    function render_thumbnail_focused(MediaFileInfo $image, $render_args)
    {
        $image->render_thumbnail(
            $this->get_css_class(1),
            "",
            $this->thumbnail_meta($image)
        );
    }

    /**
     * \brief Renders the thumbnail for a non-selected selected image
     */
    function render_thumbnail(MediaFileInfo $image, $render_args)
    {
        $image->render_thumbnail(
            $this->get_css_class(0),
            "{$this->base_uri()}{$image->slug}/",
            $this->thumbnail_meta($image)
        );
    }

    /**
     * \brief Renders the selected image
     */
    function render_focused(MediaFileInfo $image, $render_args)
    {
        $image->render($this->get_css_class(2), ["property"=>"image"], []);
        $this->print_humanmeta($image);
    }

    function get_meta_image($render_args)
    {
        if ( ! isset($render_args["image"]) )
            return parent::get_meta_image($render_args);

        return $render_args["image"]->thumbnail_url();
    }

    /**
     * \brief Template for a human-readable description of the image
     *
     * Format:
     *      %(name)         prints `name` as a property
     *      %(name:property)prints `name` as a property
     *      %(license:html) prints `license` as-is (html output)
     *      %(foo:nometa)   prints `foo` without property metadata
     */
    function humanmeta_template($image)
    {
        return "%(name) by %(author) &copy; %(copyrightYear) %(license:html)";
    }

    /**
     * \brief Renders a human-readable description of the image
     */
    function print_humanmeta($image)
    {
        $template = $this->humanmeta_template($image);
        $callback = function($matches) use ($image)
        {
            $prop = explode(".", $matches["prop"]);
            $value = $image->meta;
            foreach ( $prop as $key)
            {
                if ( !isset($value[$key]) )
                    return "";
                $value = $value[$key];
            }
            if ( is_array($value) )
                return "";

            if ( isset($matches["type"]) && !empty($matches["type"]) )
            {
                $type = $matches["type"];
            }
            else
            {
                $type = "property";
            }

            if ( $type == "html" )
                return $value;
            else if ( $type == "nometa" )
                return new SimpleElement("span", [], $value);

            return new SimpleElement("span", [$type=>$key], $value);
        };
        echo '<p class="description">';
        echo preg_replace_callback("/%\((?P<prop>[^):]+)(?::(?P<type>[^)]*))?\)/", $callback, $template);
        echo "</p>";
    }

    protected function load_image_object($filename, $slug, $title, $extension)
    {
        return new ImageInfo($this, ltrim($filename, "/"), $slug, $title, $extension);
    }
}

/**
 * \brief GalleryTrait extension that fetches images from a path
 */
trait FileGalleryTrait
{
    use GalleryTrait;

    /**
     * \brief Full path to search the image files in
     */
    protected function server_image_path()
    {
        return $this->self_dirname() . $this->media_path();
    }

    /// Extenstions to look for
    protected function image_extensions()
    {
        return array("svg", "png", "jpg", "jpeg");
    }

    /**
     * \brief Returns a human readable name out of an image file name
     */
    function extract_title($media_path)
    {
        return ucwords(preg_replace(
                array("/[0-9']+|\..*$/", "([-_/]+)"),
                array("", " "),
                $media_path
            ));
    }

    function load_images()
    {
        $image_groups = array();
        $this->load_image_dir($this->server_image_path(), "", $image_groups);
        return $image_groups;
    }

    /**
     * \brief Returns an array of basenames given the directory
     */
    protected function list_directory($directory)
    {
        return scandir($directory);
    }

    /**
     * \brief Loads images from a directory
     */
    private function load_image_dir($directory, $infix, &$groups)
    {
        $group = new ImageGroup(ucwords(trim(str_replace("/", " ", $infix))));
        $directories = [];
        foreach ( $this->list_directory($directory) as $basename )
        {
            if ( $basename == "." || $basename == ".." )
                continue;

            if ( is_dir("$directory/$basename") )
            {
                $directories []= $basename;
                continue;
            }

            foreach ( $this->image_extensions() as $ext )
            {
                $ext = ".$ext";
                if ( substr($basename, -strlen($ext)) != $ext )
                    continue;
                $slug = ltrim("$infix/".substr($basename, 0, -strlen($ext)), "/");
                $title = $this->extract_title($basename);
                $image = $this->load_image_object("$infix/$basename", $slug, $title, $ext);
                $this->load_image_metadata($image);
                $group->images []= $image;
            }
        }

        if ( !empty($group->images) )
        {
            $groups []= $group;
        }

        foreach ( $directories as $basename )
        {
            $this->load_image_dir("$directory/$basename", "$infix/$basename", $groups);
        }
    }
}

/**
 * \brief GalleryTrait extension that fetches images from a telegram gallery
 */
trait TelegramStickerGalleryTrait
{
    use GalleryTrait;

    function media_path()
    {
        return $this->telegram->file_url;
    }

    /**
     * \brief Hook called at the start of load_images()
     */
    protected function before_load()
    {
    }

    function load_images()
    {
        $this->before_load();
        $this->telegram = Telegram::instance();
        $image_groups = array();
        foreach ( $this->sets as $set )
        {
            $stricker_set = $this->telegram->sticker_set($set);
            $group = new ImageGroup($stricker_set->title);
            $group->sticker_set = $stricker_set;
            $index = 0;
            foreach ( $stricker_set->stickers as $sticker )
            {
                $file = $sticker->png();
                $file->ensure_exists();
                $image = $this->load_image_object(
                    $file->path,
                    $sticker->file_id,
                    $sticker->emoji,
                    ""
                );
                $image->sticker = $sticker;
                $image->sticker_index = $index;
                $this->load_image_metadata($image);
                $group->images[] = $image;
                $index += 1;
            }
            $image_groups[] = $group;
        }
        return $image_groups;
    }

    function render_group_title($group, $render_args)
    {
        echo mkelement([
            "h2",
            ["class"=>"gallery-title", "property" => "name"],
            $group->sticker_set->full_link()
        ]);
    }

    function humanmeta_template($image)
    {
        return "Sticker by %(author) &copy; %(copyrightYear) %(license:html)";
    }
}

/**
 * \brief GalleryTrait extension that uses a fixed set of images
 *
 * \note must reimplement load_images(), which returns an array of ImageGroup
 */
trait ExplicitGalleryTrait
{
    use GalleryTrait;

    /**
     * \brief Full path to search the image files in
     */
    protected function server_image_path()
    {
        return $this->self_dirname() . $this->media_path();
    }
}


/**
 * \brief GalleryTrait extension that fetches images from a telegram gallery
 */
trait CachedTelegramStickerGalleryTrait
{
    use GalleryTrait;

    /**
     * \brief Hook called at the start of load_images()
     */
    protected function before_load()
    {
    }

    protected function set_link($set, $attrs=[])
    {
        return mkelement(["span", [], [
            new Link($set->link, $set->title, $attrs),
            " ",
            ["span", ["class"=>"telegram-stickers-count"], [
                "(", sizeof($set->stickers), " stickers)",
            ]],
        ]]);
    }

    function load_images()
    {
        $this->before_load();
        $image_groups = array();

        global $site;
        $path = $site->settings->root_dir . $this->media_path();

        foreach ( $this->sets as $set )
        {
            $stricker_set = json_decode(file_get_contents("$path/$set.json"));
            $group = new ImageGroup($stricker_set->title);
            $group->sticker_set = $stricker_set;
            $index = 0;
            foreach ( $stricker_set->stickers as $sticker )
            {
                $image = $this->load_image_object(
                    $sticker->file->name,
                    $sticker->id,
                    $sticker->emoji,
                    ""
                );
                $image->sticker = $sticker;
                $image->sticker->set_name = $set;
                $image->sticker_index = $index;
                $this->load_image_metadata($image);
                $group->images[] = $image;
                $index += 1;
            }
            $image_groups[] = $group;
        }
        return $image_groups;
    }

    function render_group_title($group, $render_args)
    {
        echo mkelement([
            "h2",
            ["class"=>"gallery-title", "property" => "name"],
            $this->set_link($group->sticker_set)
        ]);
    }

    function humanmeta_template($image)
    {
        return "Sticker by %(author) &copy; %(copyrightYear) %(license:html)";
    }
}
