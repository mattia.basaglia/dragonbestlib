<?php

require_once(__dir__."/telegram.php");
require_once(__dir__."/site.php");

abstract class AuthSource
{
    protected $auth_key = "__auth";

    function __construct(Site $site)
    {
        $this->site = $site;
        $this->user = null;
        $this->login_age = $site->settings->auth_login_age;
        if ( !$this->login_age )
            $this->login_age = 24*60*26;

        $this->load();
        $this->user = $this->check_logged_in();
    }

    /**
     * \brief Initialize from the site settings
     */
    protected function load()
    {
    }

    /**
     * \brief Returns a string used to safely store the user to be retrieved later
     * \param $user A valid user object
     * \returns a string
     */
    abstract function serialize($user);

    /**
     * \brief Deserialize a user
     * \param $data User data as returned from serialize()
     * \returns A valid user object or \b null if \p $data is invalid or expired
     */
    abstract function deserialize($data);

    /**
     * \brief Returns user from the data provided during login
     * \param $data Login data as provided by the user (or API)
     * \returns A valid user object or \b null if \p $data is invalid
     */
    abstract protected function login_from_data($data);

    function logout()
    {
        if ( isset($_COOKIE[$this->auth_key]) )
            setcookie($this->auth_key, "", time() - 3600);
        $this->site->session->clear();
    }

    function persist($secure=True)
    {
        if ( !$this->user )
        {
            $this->logout();
        }
        $data = $this->serialize($this->user);
        setcookie($this->auth_key, $data, $this->login_age, "", "", $secure, true);
    }

    function login($data)
    {
        $this->logout();
        $this->user = $this->login_from_data($data);

        if ( !$this->user )
            return null;

        $this->site->session[$this->auth_key] = $this->serialize($this->user);
        return $this->user;
    }

    function check_logged_in()
    {
        $user = null;

        if ( isset($_COOKIE[$this->auth_key]) )
            $user = $this->deserialize($_COOKIE[$this->auth_key]);

        if ( !$user && isset($this->site->session[$this->auth_key]) )
            $user = $this->deserialize($this->site->session[$this->auth_key]);

        return $user;
    }

    function is_admin($user=null)
    {
        if ( $user === null )
        {
            $user = $this->user;
            if ( $user === null )
                return false;
        }
        return $this->on_is_admin($user);
    }

    protected function on_is_admin($user)
    {
        return false;
    }
}

class DummyAuthSource extends AuthSource
{
    function serialize($user)
    {
        return "";
    }

    function deserialize($data)
    {
        return null;
    }

    protected function login_from_data($data)
    {
        return null;
    }
}

class TelegramAuthSource extends AuthSource
{
    function load()
    {
        $this->telegram = Telegram::instance();
    }

    function serialize($user)
    {
        return json_encode($user->data);
    }

    function deserialize($data)
    {
        $decoded = json_decode($data, true);
        if ( !$decoded )
            return null;

        return TelegramAuthedUser::from_data(
                $decoded, $this->telegram, $this->login_age
            );
    }

    protected function login_from_data($data)
    {
        return TelegramAuthedUser::from_data(
            $data, $this->telegram, $this->login_age
        );
    }

    protected function on_is_admin($user)
    {
        global $site;
        return in_array($user->user_id, $site->settings->admin_user_ids, true);
    }
}
