<?php

require_once(__dir__."/html.php");
require_once(__dir__."/license.php");
require_once(__dir__."/util.php");

/**
 * \brief Base class for objects that dispatch urls to a page
 */
abstract class PageController
{
    /// Maps regex patterns to method names or callables
    public $patterns = array();

    /**
     * \brief Returns a path to search sub-pages from
     */
    abstract function base_path();

    /**
     * \brief Dispatches \p $path to one of the elements in $this->patterns
     * \return \b true if the path has been dispatched
     */
    protected function dispatch_to_patterns($path)
    {
        foreach($this->patterns as $regex => $dest)
        {
            $matches = null;
            if ( preg_match("($regex)", $path, $matches) )
            {
                if ( is_callable($dest) )
                    $dest($matches);
                elseif ( $dest instanceof PageController )
                    $dest->dispatch($path);
                else
                    $this->$dest($matches);
                return true;
            }
        }

        return false;
    }

    /**
     * \brief Handles top-level paths ("/")
     * \return \b true if the path has been handled
     */
    protected function dispatch_to_self($path)
    {
        return false;
    }

    /**
     * \brief Dispatches \p $path to external objects
     * \return \b true if the path has been dispatched
     */
    protected function dispatch_to_other($path)
    {
        return false;
    }

    /**
     * \brief Called from dispatch() when nothing matched
     */
    protected function unhandled_page($path)
    {
        http_response_code(404);
        return false;
    }

    protected function dispatch_noslash($path)
    {
        return false;
    }

    /**
     * \brief Dispatches \p $path to the right handler
     * \note Sets the HTTP response code to 404 if nothing matched
     * \return \b true if the path has been dispatched
     */
    function dispatch($path)
    {
        if ( !$path || $path[strlen($path)-1] != "/" )
        {
            if ( $this->dispatch_noslash($path) )
                return true;
        }

        if ( $path == "/" && $this->dispatch_to_self($path) )
            return true;

        if ( $this->dispatch_to_patterns($path) )
            return true;

        if ( $this->dispatch_to_other($path) )
            return true;

        return $this->unhandled_page($path);
    }

    function self_filename()
    {
        return realpath((new ReflectionClass($this))->getFileName());
    }

    function self_dirname()
    {
        return dirname($this->self_filename());
    }

}

/**
 * \brief Base page class, extend to implement pages
 */
class Page extends PageController
{
    public $scripts = array();
    public $styles = array();
    public $title = "";
    public $description = null;
    public $copy_year = "";
    public $copy_year_end = null;
    public $copy_author = "";
    public $license = "";
    public $metadata = array();
    public $template_path;
    public $template;

    function __construct()
    {
        $this->template_path = dirname(__DIR__)."/templates";
        $this->template = "{$this->template_path}/base.php";
        $this->init_patterns();
    }

    function copy_range()
    {
        $copy_year_end = $this->copy_year_end;
        if ( $copy_year_end == null )
            $copy_year_end = date("Y");
        return $copy_year_end == $this->copy_year ? $this->copy_year : "{$this->copy_year}-$copy_year_end";
    }

    /**
     * \brief For simplicity, add to $this->patterns in here
     */
    protected function init_patterns()
    {
    }

    /**
     * \brief Extra attributes as a string to add to <body>
     */
    function body_attributes($render_args)
    {
        return "";
    }

    /**
     * \brief Renders the main template
     */
    function render($render_args=array())
    {
        include($this->template);
    }

    /**
     * \return The page title
     */
    function title($render_args=array())
    {
        return $this->title;
    }

    /**
     * \brief Renders the HTML head template
     */
    function head($render_args)
    {
        include("{$this->template_path}/head.php");
    }

    /**
     * \brief Extension point for head()
     */
    function extra_head($render_args)
    {
    }

    /**
     * \brief Renders the main navigation template
     */
    function nav($render_args)
    {
        include("{$this->template_path}/nav.php");
    }

    /**
     * \brief Extension point to render custom page contents
     */
    function main($render_args)
    {
    }

    /**
     * \brief Renders the footer template
     */
    function footer($render_args)
    {
        include("{$this->template_path}/footer.php");
    }

    /**
     * \brief Extension point for footer()
     */
    function extra_footer($render_args)
    {
    }

    /**
     * \brief Renders the HTML body template
     */
    function body($render_args)
    {
        include("{$this->template_path}/body.php");
    }

    function license_object()
    {
        return License::find_license($this->license);
    }

    function base_path()
    {
        return $this->self_dirname();
    }

    protected function dispatch_to_self($path)
    {
        $this->render();
        return true;
    }

    function get_meta_image($render_args)
    {
        return null;
    }

    function get_meta_title($render_args)
    {
        return $this->title($render_args);
    }

    function get_meta_url_path($render_args)
    {
        return $_SERVER["REQUEST_URI"];
    }

    function get_meta_url($render_args)
    {
        $path = $this->get_meta_url_path($render_args);
        if ( $path === null )
            return null;
        return (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$path";
    }

    function get_meta_type($render_args)
    {
        return null;
    }

    /**
     * \return The page description (for metadata)
     */
    function get_meta_description($render_args=array())
    {
        return $this->description;
    }

    function opengraph_metadata($render_args)
    {
        global $site;
        $metadata = $site->settings->metadata;

        $meta_methods = ["image", "title", "url", "description", "type"];
        foreach ( $meta_methods as $meta )
        {
            $method = "get_meta_$meta";
            $value = $this->$method($render_args);
            if ( $value !== null )
                $metadata[$meta] = $value;
        }

        foreach($this->metadata as $name => $value)
        {
            if ( $value != null )
                $metadata[$name] = $value;
        }

        if ( isset($metadata["image"]) )
            $metadata["image"] = href($metadata["image"]);

        return $metadata;
    }

    protected function dispatch_noslash($path)
    {
        redirecto_to_slash()->dispatch($path);
        return true;
    }

    function body_title($title=null, $render_args=[], $tag="h1", $attrs=[])
    {
        if ( $title === null )
            $title = $this->title($render_args);
        $attrs = array_merge($attrs, ["property" => "name"]);
        echo mkelement([$tag, $attrs, $title]);
    }
}
