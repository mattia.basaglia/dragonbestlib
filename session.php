<?php

require_once(__dir__."/html.php");

/**
 * \brief Acts similarly to the \b $_SESSION built-in
 *        but allows lazy auto-initialization and is more self-contained
 */
class Session implements ArrayAccess
{
    const TOKEN_KEY = "csrf_token";

    function initialize($options = [], $id = null)
    {
        if ( !$this->is_initialized() )
        {
            if ( $id !== null )
                session_id($id);
            session_start($options);
        }
    }

    function rollback()
    {
        if ( $this->is_initialized() )
            session_reset();
    }

    function commit()
    {
        if ( $this->is_initialized() )
            session_write_close();
    }

    function clear()
    {
        session_unset();
    }

    function id()
    {
        return session_id();
    }

    function is_initialized()
    {
        return session_status() == PHP_SESSION_ACTIVE;
    }

    function get_unique_token()
    {
        if ( !isset($this[self::TOKEN_KEY]) )
            $this[self::TOKEN_KEY] = bin2hex(random_bytes(32));
        return $this[self::TOKEN_KEY];
    }

    function check_unique_token($token)
    {
        if ( !isset($this[self::TOKEN_KEY]) )
            return false;
        return hash_equals($token, $this[self::TOKEN_KEY]);
    }

    function check_post_token()
    {
        return $this->check_unique_token($_POST[self::TOKEN_KEY]);
    }

    function unique_token_input()
    {
        return mkelement(["input", [
            "type"=>"hidden",
            "name"=>self::TOKEN_KEY,
            "value"=>$this->get_unique_token()
        ]]);
    }

// ArrayAccess
    public function offsetExists($offset)
    {
        $this->initialize();
        return isset($_SESSION[$offset]);
    }

    public function offsetGet($offset)
    {
        $this->initialize();
        return $_SESSION[$offset];
    }

    public function offsetSet($offset, $value)
    {
        $this->initialize();
        return $_SESSION[$offset] = $value;
    }

    public function offsetUnset($offset)
    {
        $this->initialize();
        unset($_SESSION[$offset]);
    }
}
