<?php

/**
 * \brief Makes the current response into a redirect
 */
function redirect($url, $status=302)
{
    http_response_code($status);
    if ( $url != null )
        header("Location: $url");
}


/**
 * \brief Exception raised to result in a response with the given status
 */
class HttpStatus extends Exception
{
    public $code;
    function __construct($code)
    {
        $this->code = $code;
    }
}

/**
 * \brief Simple GET request with Curl
 * \returns Response body and code
 */
function curl_get($url, $opts = [])
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    $curl_version = curl_version();
    curl_setopt($ch, CURLOPT_USERAGENT, "curl/{$curl_version['version']}");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    if ( sizeof($opts) )
        curl_setopt_array($ch, $opts);
    $contents = curl_exec($ch);
    $response = curl_getinfo($ch, CURLINFO_RESPONSE_CODE);
    curl_close($ch);
    return array($contents, $response);
}
