<?php

class Settings implements ArrayAccess
{
    private $_data = array();

    function __construct($settings_filename, $defaults=array())
    {
        $this->load($settings_filename);
        foreach ( $defaults as $k => $v )
        {
            if ( !isset($this->_data[$k]) )
                $this->_data[$k] = $v;
        }
    }

    function load($settings_filename)
    {
        if ( file_exists($settings_filename) )
        {
            include($settings_filename);
            $this->_data = get_defined_vars();
        }
    }

    function __get($property)
    {
        if (property_exists($this, $property))
            return $this->$property;
        return $this->_data[$property] ?? null;
    }

    public function offsetExists($offset)
    {
        return isset($this->_data[$offset]);
    }

    public function offsetGet($offset)
    {
        return $this->_data[$property];
    }

    /// Cannot overwrite settings
    public function offsetSet($offset, $value)
    {}

    /// Cannot overwrite settings
    public function offsetUnset($offset)
    {}
}
