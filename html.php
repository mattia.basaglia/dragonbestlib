<?php

/**
 * \brief Escapes all the HTML entities in \p $text
 */
function escape($text)
{
    return htmlspecialchars($text, ENT_QUOTES|ENT_HTML5);
}

abstract class DisplayElement
{
    abstract function render();

    function __toString()
    {
        return $this->render();
    }

    /**
     * \brief Expands \p $content into an HTML string
     */
    function content_to_html($content)
    {
        if ( $content === null )
            return "";

        if ( is_string($content) )
            return escape($content);

        if ( is_array($content) )
        {
            $result = "";
            foreach ( $content as $item )
            {
                $result .= $this->content_to_html($item);
            }
            return $result;
        }

        if ( $content instanceof DisplayElement )
            return $content->render();

        return $this->content_to_html("$content");
    }
}

/**
 * \brief Just a sequence of DisplayElement rendered sequentially
 */
class DisplayElementList extends DisplayElement
{
    public $items;

    function __construct($items=[])
    {
        $this->items = $items;
    }

    function render()
    {
        return $this->content_to_html($this->items);
    }
}

/**
 * \brief Simple class to render HTML elements safely
 * \property contents If \b null, it will be an empty element.
 *                    Otherwise it can be a string, a SimpleElement or an array.
 */
class SimpleElement extends DisplayElement
{
    public $tag, $attrs, $contents;

    function __construct($tag, $attrs=array(), $contents=null)
    {
        $this->tag = $tag;
        $this->attrs = $attrs;
        $this->contents = $contents;
    }

    /**
     * \returns The element as an HTML string
     */
    function render()
    {
        return $this->open().$this->content_to_html($this->contents).$this->close();
    }

    /**
     * \returns the opening tag for the element (or the whole tag if empty)
     */
    function open()
    {
        $result = "<{$this->tag}";

        foreach ( $this->attrs as $name => $value )
        {
            $result .= " " . escape($name) . "='" . escape($value) . "'";
        }

        if ( $this->is_empty() )
        {
            $result .= "/>";
        }
        else
        {
            $result .= ">";
        }

        return $result;

    }

    /**
     * \returns the closing tag for the element (or an empty string if empty)
     */
    function close()
    {
        if ( $this->is_empty() )
            return "";
        return "</{$this->tag}>";
    }

    function is_empty()
    {
        return $this->contents === null;
    }

    function element()
    {
        return $this;
    }
}

/**
 * \brief Preformatted html string, avoids escaping entities
 */
class HtmlString extends DisplayElement
{
    public $html;

    function __construct($html)
    {
        $this->html = $html;
    }

    function render()
    {
        return $this->html;
    }
}

abstract class PageWidget extends DisplayElement
{
    /**
     * \brief Returns a SimpleElement used to render this
     */
    abstract function element();

    function render()
    {
        return $this->element()->render();
    }
}

/**
 * \brief Helper to render a link
 */
class Link extends PageWidget
{
    public $span_class = "link";
    public $uri, $name, $attrs, $mark_parent;

    function __construct($uri, $name = null, $attrs=array(), $mark_parent=True)
    {
        $this->uri = $uri;
        $this->name = $name !== null ? $name : $uri;
        $this->attrs = $attrs;
        $this->mark_parent = $mark_parent;
    }

    function is_current()
    {
        return $_SERVER['REQUEST_URI'] == $this->uri;
    }

    function is_parent()
    {
        $request_uri = $_SERVER['REQUEST_URI'];
        return strlen($this->uri) < strlen($request_uri) &&
            substr($request_uri, 0, strlen($this->uri)) == $this->uri;
    }

    function element()
    {
        if ( $this->is_current() )
            return $this->nolink();
        return $this->link();
    }

    private function apply_class(&$attrs, $class)
    {
        if ( isset($attrs["class"]) )
            $attrs["class"] .= " $class";
        else
            $attrs["class"] = $class;
    }

    function link()
    {
        $attrs = $this->attrs;
        $attrs["href"] = href($this->uri);
        if ( $this->mark_parent && $this->is_parent() )
            $this->apply_class($attrs, "link_parent");
        return new SimpleElement("a", $attrs, $this->name);
    }

    function nolink()
    {
        $attrs = $this->attrs;
        $this->apply_class($attrs, $this->span_class);
        return new SimpleElement("span", $attrs, $this->name);
    }

    static function nav_icon($name, $uri, $icon, $attrs=[], $mark_parent=True)
    {
        $class = static::class;
        $name = mkelement(["span", ["class"=>$icon], []], $name);
        return new $class($uri, $name, $attrs, $mark_parent);
    }
}

/**
 * \brief Link that doesn't check for parents
 */
class PlainLink extends Link
{
    function nolink()
    {
        return $this->link();
    }

    function is_current()
    {
        return false;
    }

    function is_parent()
    {
        return false;
    }
}

/**
 * \brief List of links
 * \note Not a linked list x3
 */
class LinkList extends PageWidget
{
    public $links = array();
    public $attrs;

    /**
     * \param $link_description An array text => url or (numeric) => object
     */
    function __construct($link_description, $class = null, $attrs=array())
    {
        $this->attrs = $attrs;
        if ( $class !== null )
            $this->attrs["class"] = $class;
        $this->extend($link_description);
    }

    function element()
    {
        $items = array();
        foreach ( $this->links as $link )
        {
            if ( $link instanceof SimpleElement && $link->tag == "li" )
                $items []= $link;
            else
                $items []= new SimpleElement("li", array(), $link->element());
        }
        return new SimpleElement("ul", $this->attrs, $items);
    }

    function add_element(DisplayElement $link)
    {
        $this->links []= $link;
    }

    function add_link($url, $name)
    {
        $this->links []= new Link($url, $name);
    }

    function add($url, $name=null)
    {
        if ( $url instanceof DisplayElement )
            $this->add_element($url);
        else
            $this->add_link($url, $name ?? $url);
    }

    function extend($link_description)
    {
        foreach ( $link_description as $name => $url )
        {
            $this->add($url, $name);
        }
    }
}

class Menu extends PageWidget
{
    public $title, $attrs, $class, $link_list;

    function __construct($title, $link_description, $class = 'sub-menu', $attrs = [])
    {
        $this->link_list= new LinkList($link_description, "$class-items");
        $this->title = $title;
        $this->attrs = $attrs;
        $this->class = $class;
        if ( ! isset($this->attrs['class']) )
            $this->attrs['class'] = '';
        else
            $this->attrs['class'] .= ' ';

    }

    function element()
    {
        $attrs = $this->attrs;
        $attrs["class"] .= $this->class;
        foreach  ( $this->link_list->links as $link )
        {
            if ( $link instanceof Link && ($link->is_current() || $link->is_parent()) )
            {
                $attrs["class"] .= ' link_parent';
                break;
            }
        }
        return new SimpleElement(
            "nav",
            $attrs,
            [
                new SimpleElement("span", ["class" => "{$this->class}-title"], $this->title),
                $this->link_list->element()
            ]
        );
    }
}

class Select extends PageWidget
{
    function __construct($options, $attrs=[], $selected=null, $is_associative=False)
    {
        if ( !$is_associative && array_keys($options) === range(0, count($options) - 1) )
            $this->options = array_combine($options, $options);
        else
            $this->options = $options;
        $this->attrs = $attrs;
        $this->selected = $selected;
    }

    function element()
    {
        $options = [];
        foreach ( $this->options as $display => $value)
        {
            $attrs = ["value"=>$value];
            if ( $this->selected == $value )
                $attrs["selected"] = "selected";
            $options []= new SimpleElement("option", $attrs, $display);
        }

        return new SimpleElement("select", $this->attrs, $options);
    }
}

class DataList extends PageWidget
{
    function __construct($options, $attrs=[])
    {
        $this->options = $options;
        $this->attrs = $attrs;
    }

    function element()
    {
        $options = [];
        foreach ( $this->options as $value)
        {
            $attrs = ["value"=>$value];
            $options []= new SimpleElement("option", $attrs);
        }
        return new SimpleElement("datalist", $this->attrs, $options);
    }
}


class Table extends PageWidget
{
    function __construct($matrix, $attrs=[])
    {
        $this->matrix = $matrix;
        $this->attrs = $attrs;
    }

    function element()
    {
        $rows = [];
        foreach ( $this->matrix as $row)
        {
            $cells = [];
            foreach ( $row as $cell )
            {
                $cells []= new SimpleElement("td", [], mkelement($cell));
            }
            $rows []= new SimpleElement("tr", [], $cells);
        }

        return new SimpleElement("table", $this->attrs, $rows);
    }
}

class HeadingAnchor extends PageWidget
{
    function __construct($tag, $title, $anchor=null)
    {
        if ( $anchor == null )
            $anchor = strtolower(preg_replace("/[^-_a-zA-Z0-9]+/", "-", $title));

        $this->tag = $tag;
        $this->title = $title;
        $this->anchor = $anchor;

    }

    function link()
    {
        return new PlainLink("#{$this->anchor}", $this->title);
    }

    function element()
    {
        return new SimpleElement(
            $this->tag,
            ["id" => $this->anchor, "class" => "heading-anchor"],
            $this->link()
        );
    }
}

/**
 * \brief HTML entity shortcut
 */
function ent($name)
{
    return new HtmlString("&$name;");
}

/**
 * \brief Smart shortcut to create nested elements
 */
function mkelement()
{
    $args = func_get_args();
    $elements = [];

    foreach ( $args as $arg )
    {
        if ( is_array($arg) )
        {
            $tag = $arg[0];
            $attrs = sizeof($arg) > 1 ? $arg[1] : [];
            $content = null;
            if ( sizeof($arg) > 2 )
            {
                $content = $arg[2];
                if ( is_array($content) )
                {
                    $content = [];
                    foreach ( $arg[2] as $item )
                        $content[] = mkelement($item);
                }
            }

            $elements[] = new SimpleElement($tag, $attrs, $content);
        }
        elseif ( $arg instanceof DisplayElement )
        {
            $elements[] = $arg;
        }
        else
        {
            $elements[] = new HtmlString($arg);
        }
    }

    if ( sizeof($elements) == 1 )
        return $elements[0];
    return new DisplayElementList($elements);
}

/**
 * \brief Converts absolute paths to proper uris
 */
function href($uri)
{
    if ( $uri )
    {
        global $site;
        if ( $uri[0] == "." )
        {
            $path = parse_url($_SERVER["REQUEST_URI"], PHP_URL_PATH);
            $uri = path_canonicalize("$path/$uri");
        }

        if ( $uri[0] == "/" )
        {
            return "{$site->settings->uri_prefix}$uri";
        }
    }
    return $uri;
}


/**
 * \brief Whether the uri is a safe for redirects (ie: an absolute internal one)
 */
function is_safe_uri_ref($uri)
{
    if ( !$uri )
        return false;

    global $site;

    if ( strpos($site->settings->uri_prefix, $uri) === 0 )
        return true;

    return $uri[0] == "/" && (strlen($uri) == 1 || $uri[1] != "/");
}

function to_safe_uri($uri, $default="/")
{
    if ( !is_safe_uri_ref($uri) )
        return $default;
    return $uri;
}

function fa_icon($icon, $style="fas", $extra_class="")
{
    return mkelement(["span", ["class"=>"fa-fw $style fa-$icon $extra_class"], []]);
}

function html_meta($property, $content)
{
    return mkelement(["meta", ["property" => $property, "content" => $content]]);
}
