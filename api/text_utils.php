<?php

class CharMetrics
{
    function __construct($right, $baseline, BoundingBox $bounds)
    {
        $this->right = $right;
        $this->baseline = $baseline;
        $this->bounds = $bounds;
    }
}

class TextWrapperCharMetrics extends CharMetrics
{
    function __construct(Imagick $image, ImagickDraw $draw, $char)
    {
        $metrics = $image->queryFontMetrics($draw, $char, false);
        $this->right = $metrics["originX"];
        $this->char = $char;
        $bl = $metrics["boundingBox"]["y2"];
        parent::__construct(
            $this->right,
            $bl,
            new BoundingBox(
                $metrics["boundingBox"]["x1"],
                -$bl,
                $metrics["boundingBox"]["x2"],
                -$metrics["boundingBox"]["y1"]
            )
        );
    }
}

function unicode_split($string)
{
    return preg_split('//u', $string, null, PREG_SPLIT_NO_EMPTY);
}

/**
 * \brief Imagick::queryFontMetrics returns garbage in boundingBox if given
 *        multiple characters, so this calls it on every character of the string
 */
class TextWrapperStringMetrics
{
    function __construct(Imagick $image, ImagickDraw $draw, $string)
    {
        if ( is_string($string) )
            $string = unicode_split($string);

        $this->char_metrics = [];
        foreach ( $string as $ch )
        {
            $this->char_metrics []= new TextWrapperCharMetrics($image, $draw, $ch);
        }
    }

    function global_metrics()
    {
        return $this->substring_metrics(0, sizeof($this->char_metrics));
    }

    function substring_metrics($offset, $length)
    {
        $object = new CharMetrics(0, 0, BoundingBox::nullbox());

        for ( $i = 0; $i < $length; $i++ )
        {
            $metrics = $this->char_metrics[$i+$offset];
            $object->bounds->grow($metrics->bounds->translated($object->right, 0));
            $object->right += $metrics->right;
            if ( $metrics->baseline > $object->baseline )
                $object->baseline = $metrics->baseline;
        }

        $object->char_metrics = array_slice($this->char_metrics, $offset, $length);

        return $object;
    }
}

class BoundingBox
{
    function __construct($x1, $y1, $x2, $y2)
    {
        $this->x1 = $x1;
        $this->x2 = $x2;
        $this->y1 = $y1;
        $this->y2 = $y2;
    }

    function __tostring()
    {
        return sprintf("%dx%d%+d%+d",
            round($this->width()), round($this->height()),
            round($this->x1), round($this->y1)
        );
    }

    static function nullbox()
    {
        return new BoundingBox(null, null, null, null);
    }

    static function from_svg(DomElement $rect_element)
    {
        $x = (float)$rect_element->getAttribute("x");
        $y = (float)$rect_element->getAttribute("y");
        $w = (float)$rect_element->getAttribute("width");
        $h = (float)$rect_element->getAttribute("height");
        return new BoundingBox($x, $y, $x + $w, $y + $h);
    }

    function is_null()
    {
        return $this->x1 === null && $this->x2 === null && $this->x2 === null && $this->y2 === null;
    }

    private function gt($a, $b)
    {
        return $a === null || ($b !== null && $a > $b);
    }

    function grow(BoundingBox $oth)
    {
        if ( $this->gt($this->x1, $oth->x1) )
            $this->x1 = $oth->x1;

        if ( $this->gt($this->y1, $oth->y1) )
            $this->y1 = $oth->y1;

        if ( $this->x2 < $oth->x2 )
            $this->x2 = $oth->x2;

        if ( $this->y2 < $oth->y2 )
            $this->y2 = $oth->y2;
    }

    function width()
    {
        return $this->x2 - $this->x1;
    }

    function height()
    {
        return $this->y2 - $this->y1;
    }

    function translated($dx, $dy)
    {
        $r = clone $this;
        $r->translate($dx, $dy);
        return $r;
    }

    function translate($dx, $dy)
    {
        $this->x1 += $dx;
        $this->y1 += $dy;
        $this->x2 += $dx;
        $this->y2 += $dy;
    }

    function intersection(BoundingBox $other)
    {
        return new BoundingBox(
            max($this->x1, $other->x1),
            max($this->y1, $other->y1),
            $this->gt($this->x2, $other->x2) ? $other->x2 : $this->x2,
            $this->gt($this->y2, $other->y2) ? $other->y2 : $this->y2
        );
    }
}

class TextWrapperLine extends CharMetrics
{
    function __construct($x, $baseline_y, $string, $line_metrics)
    {
        $this->x1 = $x - $line_metrics->bounds->x1;
        $this->string = $string;
        /// \todo is this needed?
        $this->baseline_y = $baseline_y;
        $this->char_metrics = $line_metrics->char_metrics;

        parent::__construct(
            $line_metrics->right + $this->x1,
            $baseline_y,
            $line_metrics->bounds->translated($this->x1, $baseline_y)
        );
    }

    function draw(Imagick $image, ImagickDraw $draw, $offset_x=0, $offset_y=0)
    {
        $image->annotateImage(
            $draw,
            $this->x1 + $offset_x,
            $this->baseline + $offset_y,
            0,
            $this->string
        );
    }

    function move_x($x1)
    {
        $delta = $x1 - $this->x1;
        $this->bounds->translate($delta, 0);
        $this->right += $delta;
        $this->x1 = $x1;
    }

    function move_y($baseline_y)
    {
        $delta = $baseline_y - $this->baseline_y;
        $this->bounds->translate(0, $delta);
        $this->baseline += $baseline_y;
        $this->baseline_y = $baseline_y;
    }
}

class WordSplitter
{
    function __construct(Imagick $image, ImagickDraw $draw)
    {
        $this->image = $image;
        $this->draw = $draw;
        $this->words = [];
        $this->baseline = 0;
    }

    function split_words($string)
    {
        $string = str_replace("\n", " ", $string);
        $metrics = new TextWrapperStringMetrics($this->image, $this->draw, $string);
        $this->baseline = max($this->baseline, $metrics->global_metrics()->baseline);

        $start = 0;
        $word = "";
        for ( $i = 0; $i < sizeof($metrics->char_metrics); $i++ )
        {
            $char = $metrics->char_metrics[$i]->char;
            if ( ctype_space($char) )
            {
                if ( $word )
                {
                    $this->push_word($word, $metrics, $start, $i);
                    $word = "";
                }
                $start = $i + 1;
            }
            else
            {
                $word .= $char;
            }
        }
        if ( $word )
        {
            $this->push_word($word, $metrics, $start, $i);
        }
    }

    private function push_word($word, $metrics, $start, $finish)
    {
        $word_metrics = $metrics->substring_metrics($start, $finish - $start);
        $this->words []= new TextWrapperLine(0, $this->baseline, $word, $word_metrics);
    }
}


class ImagickTextWrapper
{
    /**
     * \brief Wraps text
     * \param $x        X position
     * \param $y        Y position
     * \param $max_x    Max x position for wrapping
     */
    function __construct(Imagick $image, ImagickDraw $draw, $min_x, $min_y, $max_x)
    {
        $this->image = $image;
        $this->draw = $draw;
        $this->lines = [];
        // Bounding box limits
        $this->bounds_limit = new BoundingBox($min_x, $min_y, $max_x, null);
        // Actual bounding box
        $this->bounds = BoundingBox::nullbox();

        $this->m_metrics = $this->font_metrics("M");
        $this->next_baseline = -1;
    }

    function font_metrics($string)
    {
        return $this->image->queryFontMetrics($this->draw, $string);
    }

    /**
     * \brief Returns an ImagickTextWrapper continuing where \p $other left off
     */
    static function continue_from(ImagickTextWrapper $other)
    {
        $tw = new ImagickTextWrapper(
            $other->image, $other->draw,
            $other->bounds_limit->x1, $other->bounds->y1, $other->bounds_limit->x2
        );
        $tw->next_baseline = $other->next_baseline;
        return $tw;
    }

    /**
     * \brief Wraps text
     * \param $string    Text to render
     * \param $align     Alignment [0 left, 1 right]
     * \param $line_height Relative line height
     * \returns an array of [max_x, max_y, next_line_y, lines]
     *          where lines is an array of [x, y, text]
     */
    function wrap_text($string, $align=0, $line_height=1)
    {
        $string = unicode_split($string);
        $line_h = $this->m_metrics["textHeight"] * $line_height;
        $max_right = $this->bounds_limit->x2 ? $this->bounds_limit->width() : null;
        $metrics = new TextWrapperStringMetrics($this->image, $this->draw, $string);

        for($cursor = 0; $cursor < sizeof($string); )
        {
            while ( $cursor < sizeof($string) && ctype_space($string[$cursor]) )
                $cursor += 1;

            $right = $metrics->char_metrics[$cursor]->right;
            $x1 = $metrics->char_metrics[$cursor]->bounds->x1;
            $x2 = $metrics->char_metrics[$cursor]->bounds->x2;

            $last_space = 0;
            for ( $i = $cursor + 1; $i < sizeof($string); $i++ )
            {
                $line = array_slice($string, $cursor, $i - $cursor + 1);

                if ( $string[$i] == "\n" )
                    break;

                if ( ctype_space($string[$i]) )
                    $last_space = $i;

                $x2 = $right + $metrics->char_metrics[$i]->bounds->x2;
                $right += $metrics->char_metrics[$i]->right;

                if ( $max_right && $x2 - $x1 > $max_right )
                {
                    if ( $last_space )
                        $i = $last_space;
                    break;
                }
            }
            $line_metrics = $metrics->substring_metrics($cursor, $i - $cursor);
            $line = implode("", array_slice($string, $cursor, $i - $cursor));
            $cursor = $i;

            if ( $this->next_baseline == -1 )
                $this->next_baseline = $this->bounds_limit->y1 + $line_metrics->baseline;
            $line_start = $this->bounds_limit->x1 + ($this->bounds_limit->width() - $line_metrics->right) * $align;
            $line_obj = new TextWrapperLine($line_start, $this->next_baseline, $line, $line_metrics);
            $this->lines []= $line_obj;
            $this->bounds->grow($line_obj->bounds);
            $this->next_baseline += $line_h;
        }
    }

    /**
     * \brief Draws the lines result from wrap_text
     */
    function draw($offset_x=0, $offset_y=0)
    {
        foreach ( $this->lines as $line )
            $line->draw($this->image, $this->draw, $offset_x, $offset_y);
    }
}


/**
 * \brief Removes \p $prefix from \p $string
 * \returns \b true if \p $string starts with \p $prefix
 */
function unprefix(&$string, $prefix)
{
    if ( substr($string, 0, strlen($prefix)) !== $prefix )
        return false;

    $string = ltrim(substr($string, strlen($prefix)));
    return true;
}

function determine_font_scale(
    Imagick $image,
    ImagickDraw $draw,
    $string,            ///< Single line of text
    $width,             ///< Width to fill. If null, unbound
    $height,            ///< Height to fill. If null, unbound
    $grow_by = 0,       ///< Amount relative to $font_scale to keep around the text
    $min_scale = 0,
    $max_scale = 0
)
{
    $string_split = unicode_split($string);
    $fm = (new TextWrapperStringMetrics($image, $draw, $string_split))->global_metrics();

    if ( $width === null && $height === null )
        return max(1, $max_scale);

    if ( $fm->bounds->width() == 0 || $fm->bounds->height() == 0 )
        return $min_scale > 0 ? $min_scale : 1;

    $target_w = $width / ($fm->bounds->width() + $grow_by);
    $target_h = $height / ($fm->bounds->height() + $grow_by);

    if ( $target_w == 0 )
        $target = $target_h;
    else if ( $target_h == 0 )
        $target = $target_w;
    else
        $target = min($target_w, $target_h);

    $font_scale = max($min_scale, $target);
    if ( $max_scale )
        $font_scale = min($max_scale, $font_scale);
    return $font_scale;
}
