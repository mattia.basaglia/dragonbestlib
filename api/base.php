<?php

require_once(__dir__."/../svg.php");
require_once(__dir__."/text_utils.php");

abstract class Renderer
{
    static public $types = [
        "png" => "image/png",
        "svg" => "image/svg+xml",
        "jpg" => "image/jpeg",
        "gif" => "image/gif",
        "mp4" => "video/mp4",
    ];

    static function render_data($data, $format)
    {
        if ( $data instanceof SimpleSvgImage )
        {
            if ( $format == "svg" )
            {
                echo $data;
                return;
            }
            $data = $data->to_imagick();
        }

        if ( $data instanceof Imagick )
        {
            foreach ( $data as $frame )
            {
                $data->setImageFormat($format);
            }

            # The ffmpeg delegate has issues if it doesn't have a filename to work with
            if ( $format == "mp4" )
            {
                $basename = str_replace([".", " "], "", microtime());
                $filename = "/tmp/$basename.$format";
                try
                {
                    $data->writeImages($filename, true);
                    $fps = 100.0 / $data->getImageDelay();
                    # Imagemagick doesn't send the framerate to the ffmpeg delegate o_O
                    if ( $fps != 25 )
                    {
                        $filename_conv = "/tmp/$basename-1.$format";
                        $fac = 25 / $fps;
                        system("ffmpeg -nostdin -y -loglevel quiet -i $filename ".
                               "-filter:v \"setpts=$fac*PTS\" -r $fps $filename_conv");
                        unlink($filename);
                        $filename = $filename_conv;
                    }
                    echo file_get_contents($filename);
                }
                finally
                {
                    unlink($filename);
                }
            }
            else
            {
                echo $data->getImagesBlob();
            }

            return;
        }

        throw new HttpStatus(500);
    }

    static function render($data, $format)
    {
        header("Content-type: ".static::$types[$format]);
        static::render_data($data, $format);
    }
}

class ApiParameter
{
    function __construct(
        $name,
        $type,
        $description,
        $placeholder=null,
        $default=null,
        $choices=null
    )
    {
        $this->name = $name;
        $this->type = $type;
        $this->description = $description;
        $this->placeholder = $placeholder;
        $this->default = $default;
        $this->choices = $choices;
    }

    static function normalize($arg)
    {
        if ( $arg instanceof ApiParameter )
            return $arg;
        return new ApiParameter(...$arg);
    }

    private function fetch_bool($param_data)
    {
        $found = isset($param_data[$this->name]);
        return $this->default ? !$found : $found;
    }

    private function fetch_int($param_data)
    {
        return isset($param_data[$this->name]) && $param_data[$this->name] != "" ? (int)$param_data[$this->name] : $this->default;
    }

    private function fetch_float($param_data)
    {
        return isset($param_data[$this->name]) && $param_data[$this->name] != "" ? (float)$param_data[$this->name] : $this->default;
    }


    private function fetch_multiline($param_data)
    {
        return $this->fetch_string($param_data);
    }

    private function fetch_string($param_data)
    {
        $val = "";
        if ( isset($param_data[$this->name]) )
            $val = trim($param_data[$this->name]);
        return $val !== "" ? $val : $this->default;
    }

    private function fetch_raw($param_data)
    {
        return $param_data[$this->name] ?? null;
    }

    private function fetch_color($param_data)
    {
        if ( isset($param_data[$this->name]) && $param_data[$this->name] )
            try {
                return new ImagickPixel($param_data[$this->name]);
            } catch ( ImagickPixelException $e ) {}
        if ( $this->default instanceof ImagickPixel )
            return $this->default;
        return new ImagickPixel($this->default);
    }

    private function fetch_datetime($param_data)
    {
        if ( $this->default instanceof DateTime )
            $datetime = $this->default;
        else
            $datetime = new DateTime();

        if ( isset($param_data[$this->name]) )
        {
            $time = strtotime($param_data[$this->name]);
            if ( $time )
            {
                $datetime->setTimestamp($time);
                return $datetime;
            }
        }
        if ( $this->default && !($this->default instanceof DateTime) )
            $datetime->setTimestamp(strtotime($this->default));
        return $datetime;
    }

    static function font_path($font="")
    {
        global $site;
        $fullpath = path_join($site->settings->root_dir, $site->settings->api_font_path);
        if ( strlen($font) > 0 )
            $fullpath .= "/$font.ttf";
        if ( file_exists($fullpath) )
            return $fullpath;
        return null;
    }

    private function fetch_font($param_data)
    {
        $font = $this->fetch_raw($param_data);
        if ( $font )
        {
            $fullpath = $this->font_path($font);
            if ( $fullpath )
                return $fullpath;
            if ( in_array($font, Imagick::queryFonts()) )
                return $font;
        }

        $fullpath = $this->font_path($this->default);
        if ( $fullpath )
            return $fullpath;
        return $this->default;
    }

    function fetch($param_data)
    {
        $att = "fetch_{$this->type}";
        $value = method_exists($this, $att) ? $this->$att($param_data) : $att($this->fetch_raw($param_data));
        if ( $this->choices && !in_array($value, $this->choices) )
            return $this->default;
        return $value;
    }

    private function choice_wrapper($x)
    {
        return "<span class='api-param-choice'>$x</span>";
    }

    function description()
    {
        $desc = "<dt><span class='api-param'>{$this->name}</span>";
        if ( $this->choices )
        {
            $desc .= "=<span>".implode("", array_map([$this, "choice_wrapper"], $this->choices))."</span>";
        }
        else if ( $this->placeholder )
        {
            $desc .= "=<span class='api-placehoder'>{$this->placeholder}</span>";
        }
        else if ( $this->default and $this->type != "bool" )
        {
            $desc .= "=<span class='api-default'>{$this->default}</span>";
        }
        return "$desc</dt><dd>{$this->description}</dd>";
    }

    function form_input($id_prefix)
    {
        $id = "{$id_prefix}_{$this->name}";

        if ( $this->choices )
            return new Select($this->choices, ["name"=>$this->name, "id"=>$id], $this->default);

        $placehoder = $this->placeholder ? $this->placeholder : $this->name;
        $attrs = [];

        if ( $this->type == "bool" )
        {
            $type = "checkbox";
        }
        else if ( $this->type == "int" )
        {
            $type = "number";
        }
        else if ( $this->type == "float" )
        {
            $type = "number";
            $attrs["step"] = "any";
        }
        else if ( $this->type == "datetime" )
        {
            $type = "time";
        }
        else if ( $this->type == "color" )
        {
            return mkelement(
                ["input",  ["type"=>"text", "name"=>$this->name, "id"=>$id, "placeholder"=>$placehoder]],
                ["input", ["type"=>"checkbox", "onchange"=>"document.getElementById('$id').setAttribute('type', this.checked ? 'color' : 'text');"]]
            );
        }
        else if ( $this->type == "multiline" )
        {
            return mkelement(
                ["textarea",  ["name"=>$this->name, "id"=>$id, "placeholder"=>$placehoder, "rows"=>3], []]
            );
        }
        else
        {
            $type = "string";
        }

        return mkelement(
            ["input",  array_merge($attrs, ["type"=>$type, "name"=>$this->name, "id"=>$id, "placeholder"=>$placehoder])]
        );
    }

    function form_input_label($id_prefix)
    {
        $id = "{$id_prefix}_{$this->name}";

        return mkelement(["label", ["for"=>$id], $this->description]);
    }
}


class ApiBasePage
{
    /// lists format shorthands
    public $formats = [];
    public $description = null;
    public $params = [];
    public $main_page = null;
    public $params_example = "";
    private $param_data = [];
    private $live_page = false;

    function __construct()
    {
        $params = [];
        foreach ( $this->params as $p )
        {
            $param = ApiParameter::normalize($p);
            $params[$param->name] = $param;
        }
        $this->params = $params;
    }

    protected function set_status_code($code)
    {
        if ( $this->live_page )
            http_response_code($code);
    }

    function dispatch($format)
    {
        if ( !in_array($format, $this->formats) )
            return false;

        $this->param_data = $_GET;
        $this->live_page = true;
        $data = $this->fetch_data($format);
        $this->encode_data($data, $format);
        return true;
    }

    /**
     * \brief Get the data as from a request but without side effects (like
     *        setting the HTTP response code) and using arbitrary parameters
     */
    function get_data($params, $format=null)
    {
        $this->param_data = $params;
        $this->live_page = false;
        return $this->fetch_data($format);
    }

    function get_parameter($name)
    {
        return $this->params[$name]->fetch($this->param_data);
    }

    function fetch_data($format)
    {
        return null;
    }

    function encode_data($data, $format)
    {
        throw HttpStatus(404);
    }
}


class MemeImageBasePage extends ApiBasePage
{
    function encode_data($data, $format)
    {
        if ( $data === null )
            throw HttpStatus(404);
        Renderer::render($data, $format);
    }

    function is_animated($format)
    {
        switch ( $format )
        {
            case "gif":
            case "mp4":
                return true;

            default:
                return false;
        }
    }
}

class JsonBasePage extends ApiBasePage
{
    public $formats = ["json"];

    function encode_data($data, $format)
    {
        header("Content-type: application/json");
        echo json_encode($data);
    }

    function fetch_data($format)
    {
        return [];
    }

    function get_body($assoc = true)
    {
        return json_decode(file_get_contents("php://input"), $assoc);
    }
}

function parse_colorize(&$string, &$colorize)
{
    $space = strpos($string, " ");
    if ( $space === false )
    {
        $space = strlen($string);
    }
    else
    {
        $o_bracket = strpos($string, "(");
        $c_bracket = strpos($string, ")");
        if ( $o_bracket !== false && $c_bracket !== false && $o_bracket < $space )
            $space = $c_bracket + 1;
    }

    $color = substr($string, 0, $space);
    $string = ltrim(substr($string, $space));

    try {
        $colorize = new ImagickPixel($color);
    } catch ( ImagickPixelException $e ) {
    }
}

function colorize_params(ImagickPixel $src_color, ImagickPixel $dst_color)
{
    $src_hsl = $src_color->getHSL();
    $target_hsl = $dst_color->getHSL();

    $deltahue = $target_hsl["hue"] - $src_hsl["hue"];
    $im_deltahue = 100 + $deltahue * 200;

    if ( $src_hsl["saturation"] == 0 )
        $im_deltasat = 0;
    else
        $im_deltasat = 100 * $target_hsl["saturation"] / $src_hsl["saturation"];

    if ( $src_hsl["luminosity"] == 0 )
        $im_deltabri = 0;
    else
        $im_deltabri = 100 * $target_hsl["luminosity"] / $src_hsl["luminosity"];

    return [$im_deltabri, $im_deltasat, $im_deltahue];
}


class Rainbowizer
{
    function __construct($width, $height, $rainbow_hex, $direction=-1)
    {
        $this->images = [];
        $this->gradient = new Imagick();
        $this->gradient->newPseudoImage(
            $height,
            $width,
            "gradient:$rainbow_hex"
        );
        $this->gradient->setImageColorspace(Imagick::COLORSPACE_HSV);
        $this->gradient->transformImageColorspace(Imagick::COLORSPACE_RGB);
        $this->gradient->rotateImage('black', 90 * ($direction > 0 ? 1 : -1));
    }

    function add_image(Imagick $image)
    {
        $new = clone $image;
        $this->images[spl_object_hash($image)] = $new;
        return $new;
    }

    function apply(Imagick $image, $x, $y)
    {
        $copy = $this->images[spl_object_hash($image)] ?? $this->add_image($image);
        $copy->compositeImage($this->gradient, Imagick::COMPOSITE_HUE, $x, $y);
        $image->compositeImage($copy, Imagick::COMPOSITE_ATOP, 0, 0);
    }
}
