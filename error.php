<?php

/**
 * \brief Friendly wrapper over errors reported by PHP
 */
class PhpError
{
    static $codes;
    public $type, $message, $file, $line;

    function __construct($type, $message, $file, $line)
    {
        $this->type = $type;
        $this->message = $message;
        $this->file = $file;
        $this->line = $line;
    }

    /**
     * \brief String associated with the error type
     */
    function type_name()
    {
        return self::$codes[$this->type];
    }

    /**
     * \brief Returns the last error
     */
    static function get_last()
    {
        $last_error = error_get_last();
        if ( !$last_error )
            return null;
        return new PhpError(
            $last_error['type'],
            $last_error['message'],
            $last_error['file'],
            $last_error['line']
        );
    }

    function __tostring()
    {
        return "{$this->file}:{$this->line}: {$this->type_name()}: {$this->message}";
    }

    /**
     * \brief Logs the error
     */
    function log()
    {
        error_log("$this");
    }

    /**
     * \brief Whether the error should be reported
     */
    function should_report()
    {
        return error_reporting() & $this->type;
    }

}

/**
 * \brief Error codes as an associative array [type => name]
 */
PhpError::$codes = array_flip(array_slice(get_defined_constants(true)['Core'], 0, 15, true));
