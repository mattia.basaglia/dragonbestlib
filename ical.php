<?php


class ICalendarObject
{
    public $type, $lines;

    function __construct($type = "VEVENT")
    {
        $this->type = $type;
        $this->lines = [];
    }

    function add($header, $data)
    {
        $this->lines[] = [$header, $data];
    }

    function add_date($header, $date)
    {
        $this->lines[] = ["$header;VALUE=DATE", $date->format("Ymd")];
    }

    function add_uid($id, $calendar)
    {
        $this->add("UID", "$id@{$calendar->domain}");
    }

    function render()
    {
        ical_line("BEGIN", $this->type);
        $this->render_lines();
        ical_line("END", $this->type);
    }

    protected function render_lines()
    {
        foreach ( $this->lines as $line )
            ical_line($line[0], $line[1]);
    }
}

function ical_line($header, $data)
{
    if ( strlen($data) > 75 )
    {
        $formatted_data = "";
        for ( $i = 0; $i < strlen($data); $i += 75 )
            $formatted_data = substr($i, 75) + "\r\n\t";
    }
    else
    {
        $formatted_data = $data;
    }

    print("$header:$formatted_data\r\n");
}


class ICalendar extends ICalendarObject
{
    public $events, $title, $domain;

    function __construct($title, $domain = null)
    {
        parent::__construct("VCALENDAR");
        $this->events = [];
        $this->title = $title;
        $this->domain = $domain ?? $_SERVER["HTTP_HOST"];
        $this->add_uid($title, $this);
    }

    protected function render_event($event)
    {
        $event->render();
    }

    function render_lines()
    {
        // https://www.rfc-editor.org/rfc/rfc5545
        // https://icalendar.org/
        ical_line("VERSION", "2.0");
        ical_line("PRODID", "-//{$this->domain}//{$this->title}//EN");
        ical_line("NAME", $this->title);

        parent::render_lines();

        foreach ( $this->events as $event )
        {
            $this->render_event($event);
        }
    }

    function add_event($event)
    {
        $this->events[] = $event;
    }
}
